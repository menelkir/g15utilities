Part of the Logitech LCD SDK, Copyright Logitech 2005
----------------------------------------------------------


Logitech LCD SDK sample: simple


This sample shows usage of most functions in the library,
their sequence of calling and how to get access to an LCD
display.

It is a simple console application that performs the steps
to connect, enumerate and open a device, then places a static
bitmap onto the display, and finally exits.
