//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by framework.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FRAMEWORK_DIALOG            102
#define IDS_APPNAME                     102
#define IDR_MAINFRAME                   128
#define IDB_BTN_DN                      130
#define IDC_TEXT                        1003
#define IDC_FONT                        1004
#define IDC_FONT_DESCRIPTION            1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
