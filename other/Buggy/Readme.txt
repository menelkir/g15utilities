Buggy v0.1
-----------

Istallation:
------------

To run Buggy, simply copy the dll files to your windows\system32 folder, and the exe to any desired folder and double click it.  A file will be created in this folder to save information about your virtual pet.

If you get Run-time error 429: "ActiveX component can't create object", run the setup file in the 'Buggy Installation" folder, delete the "Buggy.exe" file created by the setup and replace it with "Buggy v0.21.exe".  This should fix the problem.


How to care for Buggy:
----------------------

At the bottom of the screen are three bars to show you Buggy's stats.  By monitoring these three bars you will be able to make an excellent caretaker for Buggy!

1)Hunger:

This bar indicates how hungry Buggy is. A low value means that he needs food. Button 4 will put a.... pear(?)... in his cage which he will detect and eat. (Button functions can be viewd by pressing button 1. Press button 1 again to return to the previous screen).Feeding Buggy is the only way in which the hunger bar can be boosted.
WATCH OUT!! Buggy loves food!! and he has no problem at all eating himself to death!! Take care not to feed buggy when he isn't really hungry as this will damage his health.

2)Energy:

This bar indicates how much energy Buggy currently has. When this value gets too low, Buggy will start taking short rests, until he can no longer stay awake.  If Buggy feels it is necessary he take a nap on his own.
Buggy's enery can be boosted by playing with him (button 3). If buggy wants to play, he will get an energy boost and start running around. If buggy is too tired, or not in a good mood (discussed later), he will instead try to get away from you by runing a short distance.
the latter will also cause his energy to decrease insMoodtead of boosting it!!

3)Mood:

This bar indicates how happy Buggy is with his owner.  This bar does not indicate Buggy's overall mood. For instance:  if buggy is hungry he will get sad, but the mood bar won't be affected until he is neglected (if he isn't fed for too long, or if his cage is dirty).  Buggy's overall mood can be determined by looking at his face.
Buggy will like you more when you play with him while he isn't tired, or already think you suck!
If the mood bar is too low buggy will react in the same way as he would when he didn't have enough energy to play.

-------------------

In addition to the three bars discussed above, there is also a health indicator in the form of a heart in the top right corner of the screen.  This indicator tells you how healthy Buggy is. If this value is 0, it means Buggy has died.
Buggy's health will be affected if he isn't fed according to his needs, or if his cage is dirty, but it will regenerate when the cage is cleaned and he is fed - provided his energy is above 40%.
Buggy will die within approximately 9 - 15 hours if left unattended, dpending on his size and how active he is during this time.

Note:Buggy has three stages of growth and cannot show emotion during the first.


~eNjOy!!!

--------------------

Version History
---------------

v0.21
-----
-Fixed a bug which would cause the application to crash when food is rappidly put into and removed from Buggy's cage.
-fixed a bug which caused Buggy to still gain energy after he died and prevented the "Buggy is dead" notification message from popping up.
(Thaks to cowkilla for his bug reporting and support!)

v0.2
-----
-Fixed a bug where your buggy's name would be forgotten.
-You can now change your buggy's name by right-clicking the tray icon and selecting "Change Name" (this will exit buggy as the change will only take place after Buggy is restarted)

v0.13
-----
-Fixed a bug where Buggy couldn't reach his food in his final stage of growth.

v0.12
-----

-Fixed a bug where an overflow error would occur in Buggy's final stage of growth.

v0.11
-----

-NEW: Buggy's mood bar will now regenerate when he get's attended to after being neglected.
-Buggy's mood is now even more affected when he gets hungry.
-Changed the amount of health Buggy has to half of what it was.  You should see Buggy lose Health when he gets neglected now.


--------------------
SnagaDuath