POP3 with SSL Readme

1. OVERVIEW
2. INSTALLATION & CONFIGURATION
3. RUNNING STUNNEL
4. LCD POP3 MONITOR CONFIGURATION
5. GMAIL

****************************************************************************
1. Overview
 Some servers are SSL enabled to access POP3 email, and some like Gmail even 
 require it. To connect to a POP3 server using SSL it is recommended to use 
 a program called STunnel (http://www.stunnel.org). 

****************************************************************************
2. Installation & Configuration
 Once you download the STunnel package, you will need to install it to a 
 directory of your choice.  You will then need to edit the stunnel.conf file
 that is in the installation folder.  See the example below:

 Example:
 To connect to Gmail, write the following in the config file:

 client = yes
 debug = debug

 [pop3s]
 accept = 127.0.0.1:1109
 connect = pop.gmail.com:995

 [smtps]
 accept = 127.0.0.1:259
 connect = smtp.gmail.com:465

****************************************************************************
3. Running STunnel
 Launch stunnel.exe which is located in the installation folder. After starting 
 the program you can configure the POP3 Monitor to connect to STunnel which 
 in turn is going to connect to the server specified in the coonfig file via 
 SSL.

****************************************************************************
4. LCD POP3 Monitor Configuration
 From the LCD POP3 Monitor configuration window, set the following:

 Mail Server: 127.0.0.1
 Port Number: 1109
 User Name: <your gmail user name>
 Password: <your gmail password>

****************************************************************************
5. Gmail
 POP3 email access is not enabled by default in Gmail. You must go into the 
 Gmail settings to enable it. I this is not done, the LCD POP3 Monitor applet
 will not be able to retrieve your emails.

