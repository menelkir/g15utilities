Fichier Lisez-moi du logiciel pour claviers G-series de Logitech

(c) 2004-2006 Logitech.  Tous droits r�serv�s.  Logitech, le logo 
Logitech et les autres marques Logitech sont la propri�t� de Logitech 
et sont susceptibles d'�tre d�pos�s.
Toutes les autres marques sont la propri�t� exclusive de leurs d�tenteurs 
respectifs.

SOMMAIRE DU FICHIER LISEZ-MOI

1.  PRESENTATION
 1.1 SYSTEMES D�EXPLOITATION PRIS EN CHARGE

2.  INSTALLATION ET SUPPRESSION DU LOGICIEL 
 2.1    INSTALLATION
 2.2    DESINSTALLATION

3.  Configuration de l�application Logitech G-Series Media Display
 3.1  Activer le LCD
 3.2  Activer l�ex�cution en t�che de fond
 3.3  Touches multim�dia
  3.3.1  Winamp
  3.3.2  WinDVD

4.  Horloge LCD G-series de Logitech
 4.1  Clients de messagerie �lectronique pris en charge

5.  Chronom�tre � rebours sur �cran LCD G-series de Logitech
 5.1  Libell�s du chronom�tre
 5.2  Modes du chronom�tre
 5.3  Commandes du chronom�tre

6.  Ecran POP3 LCD G-series de Logitech
 6.1 Configuration
  6.1.1 Param�tres POP3
  6.1.2 Param�tres d'authentification
  6.1.3 Tester la connexion 
  6.1.4 Acc�s au courrier �lectronique
  6.1.5 Raccourci de l'application de messagerie �lectronique POP3
 6.2 Op�ration
  6.2.1 Lancer l'application de messagerie �lectronique
  6.2.2 Synchroniser
  6.2.3 Parcourir les messages
  6.2.4 Marquer pour la suppression

7.  Onglets LCD G-series de Logitech

8.  REMARQUES SPECIFIQUES AUX DISPOSITIFS

9.  REMARQUES SPECIFIQUES AUX JEUX

10.  DEPANNAGE
 10.1    PROGRAMMES TIERS D�INSTALLATION

11.  CONTACTER LE SERVICE CLIENTELE

****************************************************************************

1. NOUVEAUTES DE CETTE VERSION

1.1 SYSTEMES D�EXPLOITATION PRIS EN CHARGE

 Le logiciel Logitech G-series Keyboard est con�u pour fonctionner sous 
 Windows XP uniquement.

****************************************************************************

2. INSTALLATION ET DESINSTALLATION DU LOGICIEL SOUS WINDOWS XP

2.1 INSTALLATION RAPIDE

 Windows peut �tre relanc� au cours de la proc�dure d'installation. Nous vous 
 recommandons d'enregistrer votre travail et de fermer tous les programmes 
 avant de d�marrer l'installation.

 Il n'est pas n�cessaire de d�sinstaller les versions pr�c�dentes du logiciel 
 avant l'installation. Le programme d�installation v�rifie s�il existe des 
 versions pr�c�dentes et supprime ou met � jour les fichiers le cas �ch�ant.

 Pour commencer l'installation, ins�rez le CD du logiciel dans votre lecteur 
 de CD-ROM. La fen�tre d�installation s�affiche quelques secondes (si vous 
 avez activ� AutoPlay sur votre syst�me), suivez alors les instructions 
 affich�es � l��cran.

 Si la fonction AutoPlay de Windows XP n'a pas �t� activ�e, cliquez sur 
 D�marrer, s�lectionnez la commande Ex�cuter, cliquez sur Parcourir pour 
 s�lectionner le lecteur de CD-ROM, double-cliquez sur le programme 
 SETUP.EXE qui se trouve sur le CD, puis suivez les instructions qui 
 s'affichent dans cette fen�tre.

2.2    DESINSTALLATION

 Le logiciel peut �tre d�sinstall� de Windows en cliquant sur l�ic�ne 
 Ajout/Suppression de programmes du Panneau de configuration de Windows XP. 
 S�lectionnez "Logitech G-series Keyboard Software" et cliquez sur le bouton 
 "Ajouter/Supprimer"�  Suivez les instructions qui s'affichent � l'�cran 
 pour terminer la proc�dure de d�sinstallation.

****************************************************************************

3. ECRAN MEDIA DISPLAY

 Pour configurer le module Logitech G-series Media Display:

 1. Faites un clic droit sur l�ic�ne de Logitech G-series Keyboard Profiler 
 dans la barre d��tat syst�me et choisissez le panneau de configuration de 
 l��cran.
 2. Dans le gestionnaire LCD Logitech G-series, choisissez Logitech G-series 
 Media Display dans la liste, puis cliquez sur le bouton Configurer.

3.1  Activer le LCD
 L�option Activer le LCD sert � d�terminer s�il faut oui ou non afficher les 
 informations multim�dia du lecteur sur l��cran (pour les lecteurs pris 
 en charge uniquement). Les lecteurs multim�dia suivants sont pris en 
 charge par cette fonction:
 - iTunes
 - RealPlayer
 - Sonique
 - Winamp
 - Music Match
 - Windows Media Player 9 (et versions ult�rieures)
 - MediaLife

3.2  Activer l�ex�cution en t�che de fond
 L�option Activer l�ex�cution en t�che de fond permet � certains lecteurs 
 (pris en charge) d�utiliser les touches multim�dia du clavier en t�che 
 de fond.  Contrairement aux autres lecteurs pris en charge, ceux-l� ne 
 fonctionnent pas ainsi par d�faut.  Les lecteurs multim�dia suivants 
 sont pris en charge par cette fonction:

 - iTunes 4.7
 - RealPlayer 10
 - Sonique

3.3  Touches multim�dia

3.3.1  Winamp
 Pour activer les touches multim�dia dans Winamp, vous devez activer 
 l�option Raccourcis g�n�raux dans les pr�f�rences Winamp.  Veuillez vous 
 reporter � l�aide Winamp pour plus d�informations.

3.3.2  WinDVD
 Lorsque l'application Logitech G-series Media Display est en cours 
 d'ex�cution, les touches multim�dia fonctionnent pour WinDVD lorsque l'une 
 de ses fen�tres se trouve au premier plan. 

****************************************************************************

4.0 Horloge LCD G-series de Logitech
 Ce module affiche la date et l�heure du jour, ainsi que le nombre total de 
 courriers �lectroniques non lus pour les comptes pris en charge par les 
 clients de messagerie �lectronique.

4.1  Clients de messagerie �lectronique pris en charge
 Microsoft Outlook Express
 Microsoft Outlook 2003
 Thunderbird
 MSN Hotmail (uniquement lorsque MSN Messenger est actif).

****************************************************************************

5.  Chronom�tre � rebours sur �cran LCD G-series de Logitech
 A sa premi�re installation, LCDCountdown affiche par d�faut les libell�s 
 et les valeurs.  Pour les modifier, acc�dez au panneau de configuration  de 
 l'�cran, s�lectionnez le chronom�tre � rebours de l'�cran LCD, puis cliquez 
 sur Configurer.

 Choisissez en premier lieu si vous voulez utiliser un ou deux retardateurs.  
 Vous pouvez activer ou d�sactiver le Retardateur 1 ou 2 en cliquant sur la 
 case � cocher.

5.1  Libell�s du chronom�tre
 Pour modifier le libell� du chronom�tre, ouvrez la configuration et changez 
 la valeur du champ Libell�:.

5.2  Modes du chronom�tre
 Sous le libell� du chronom�tre se trouve l'option de mode.

 Le mode Chronom�tre ex�cute un comptage � la mani�re d'un chronom�tre
 Le mode Chronom�tre � rebours n�cessite d'entrer une valeur � partir de 
 laquelle effectuer le d�compte.

 Trois cases permettent de saisir les heures, les minutes et les secondes.
 Vous pouvez recourir � deux options suppl�mentaires.  La premi�re, Boucle, 
 permet de red�marrer le chronom�tre � rebours automatiquement lorsque 
 celui-ci atteint z�ro.  La seconde permet de lire un son � la fin du 
 d�compte.  Cochez la case pour activer la lecture d'un son, puis recherchez 
 un fichier son � partir du bouton �...�.

5.3  Commandes du chronom�tre
 Appuyez sur le bouton situ� sous le symbole souhait�: le triangle lance 
 le d�compte, les deux barres enclenchent la pause et la barre/fl�che 
 r�initialise le compteur (symboles universels).  Si la pause est 
 enclench�e, le bouton correspondant prendra la forme d'un bouton de 
 d�marrage.  Lorsque le chronom�tre � rebours atteint z�ro et que vous 
 n'�tes pas en mode Boucle, vous pouvez uniquement r�initialiser le 
 compteur.

****************************************************************************

6.  Ecran POP3 LCD G-series de Logitech
 Cet �cran vous permet de r�cup�rer et de pr�visualiser les en-t�tes de 
 messages �lectroniques de votre compte POP3 pr�f�rentiel.

6.1 Configuration
 Au premier lancement, l'applet devra �tre configur�e pour pouvoir communiquer 
 avec le compte POP3 de votre choix.  La bo�te de dialogue Param�tres est 
 accessible via les boutons de l'�cran LCD en cliquant sur celui 
 correspondant � l'ic�ne en forme d'outil.

 6.1.1 Param�tres POP3
  Serveur de messagerie � Entrez l'adresse du serveur POP3.
  Num�ro de port � Entrez le num�ro de port du serveur de messagerie POP3.

 6.1.2 Param�tres d'authentification
  Nom d'utilisateur � Entrez le nom d'utilisateur du compte POP3.
  Mot de passe � Entrez le mot de passe � utiliser pour l'acc�s au compte POP3.

 6.1.3 Tester la connexion 
  Une fois que le compte POP3 � utiliser a �t� configur�, vous pouvez tester 
 les param�tres en appuyant sur le bouton Tester la connexion.

 6.1.4 Acc�s au courrier �lectronique
  Vous pouvez d�finir l'intervalle dans laquelle l'applet doit rechercher 
 les nouveaux messages sur le compte POP3.

 6.1.5 Raccourci de l'application de messagerie �lectronique POP3
  Vous pouvez d�finir le bouton de messagerie LCD pour lancer l'application 
 de messagerie �lectronique par d�faut ou une autre application.  Pour 
 d�finir une application personnalis�e, cliquez sur le bouton de navigation 
 et choisissez le fichier ex�cutable de l'application � utiliser.

6.2 Op�ration
 6.2.1 Lancer l'application de messagerie �lectronique
  Le bouton signal� par enveloppe lance l'application de messagerie 
 �lectronique s�lectionn�e dans les param�tres de configuration de l'applet.

 6.2.2 Synchroniser
  Le bouton signal� par les deux fl�ches permet de synchroniser le compte 
 de messagerie �lectronique POP3.

 6.2.3 Parcourir les messages
  Les boutons signal�s par des fl�ches droite et gauche permettent de passer 
 d'un message � un autre.

 6.2.4 Marquer pour la suppression
  Le bouton signal� par une croix (X) permet de marquer le message affich� 
 pour la suppression.
  

****************************************************************************

7.  Onglets LCD G-series de Logitech
 Par d�faut, le bouton suppl�mentaire permet de parcourir les applets LCD.
 Pour obtenir le mode des onglets, maintenez le bouton suppl�mentaire pendant 
 3 secondes. En mode des onglets, le bouton rond pr�programm� permet de 
 parcourir diff�rents onglets de configuration.

7.1  S�lections d'onglets
 Les diff�rents onglets sont:
 - Passer au programme
 - Algorithme de changement
 - Mode de changement rapide
 - Luminosit�
 - Contraste
 - Dur�e d'affichage
 
 7.1.1 Passer au programme
  Passe d'une applet LCD active � une autre.

 7.1.2 Algorithme de changement
  - Manuel (utiliser les commandes LCD):
    Utilisez les boutons � l'�cran pour passer d'une applet � une autre.
  - Manuel (autoriser l'affichage de priorit� haute):
    Identique au mode pr�c�dent, � la diff�rence qu'il permet aux applets 
 de priorit� haute de s'imposer.
  - Rotation entre les programmes - en secondes
    Parcourt chaque applet LCD active.
  - Rotation en utilisant la technologie Klever-VU
    Ce mode fait appel � un algorithme qui alloue les plages de temps 
  disponibles aux applets de fa�on intelligente.

 7.1.3 Mode de changement rapide
  Ce mode permet de passer d'une applet � la suivante sans afficher le nom 
  des applets actives en premier.
  - D�sactiver
  - Activer

 7.1.4 Luminosit�
  Il existe trois r�glages de luminosit�: Haute, Moyenne et Basse

 7.1.5 Contraste
  Il existe trois r�glages de contraste: Haut, Moyen et Bas

 7.1.6 Dur�e d'affichage
  Ce mode d�finit le nombre de secondes attribu� � chaque applet en mode de 
  rotation.  La dur�e peut �tre d�finie de 1 � 20�secondes.
 
****************************************************************************

8. REMARQUES SPECIFIQUES AUX DISPOSITIFS

****************************************************************************

9. REMARQUES SPECIFIQUES AUX JEUX

****************************************************************************

10. DEPANNAGE

 Cette section d�crit les solutions possibles pour certains probl�mes 
 sp�cifiques que vous pourriez rencontrer.
 Vous pouvez �galement consulter la section D�pannage de l'aide en ligne de 
 Logitech G-series Keyboard Profiler.

10.1    PROGRAMMES TIERS D�INSTALLATION

Nous vous sugg�rons d�utiliser le programme de d�sinstallation Windows 
 plut�t que celui des tiers. Si, au cours de l'installation du logiciel 
 Logitech G-series Keyboard, des conflits avec un programme de surveillance 
 interviennent, quittez l'installation, d�sactivez le programme de 
 surveillance et r�installez le logiciel Logitech G-series Keyboard.

****************************************************************************

11. CONTACTER LE SERVICE CLIENTELE

Site Web: http://www.logitech.com

Le site Web de Logitech comporte, entre autres, un service client�le 
interactif, des correctifs et des informations sur les produits.

Site FTP: ftp://ftp.logitech.com

Le site FTP comporte des correctifs et des mises � jour des programmes de 
gestion de nos produits.

Assistance informatique�: http://www.logitech.com/support

Posez des questions aux techniciens qualifi�s de notre Service client�le par 
courrier �lectronique.

Service client�le:
    +1 702-269-3457 (USA) (lundi � vendredi, 6h00 � 18h00, heure standard du 
    Pacifique)
    +1�416-207-2782 (Californie) (lundi � vendredi, 6h00 � 18h00, heure 
    standard du Pacifique)
    (Remarque: les horaires peuvent �tre soumis � changement)

    L��quipe d�assistance technique peut r�pondre � toute question technique 
    ou de service pour les produits Logitech.

----------------------------------------------------------------------------
(c) 2004-2006 Logitech.  Tous droits r�serv�s.  Logitech, le logo Logitech 
et les autres marques Logitech sont la propri�t� de Logitech et sont 
susceptibles d'�tre d�pos�s.
Toutes les autres marques sont la propri�t� exclusive de leurs d�tenteurs 
respectifs.

