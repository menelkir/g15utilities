Logitech G-series Keyboard Software README File

(c) 2004-2006 Logitech.  All rights reserved.  Logitech, the Logitech logo,
and other Logitech marks are owned by Logitech and may be registered.
All other trademarks are the property of their respective owners.

README TABLE OF CONTENTS

1.  OVERVIEW
 1.1  SUPPORTED OPERATING SYSTEMS

2.  INSTALLING AND REMOVING THE SOFTWARE
 2.1  INSTALLATION
 2.2  UNINSTALL

3.  Configuring the Logitech G-series Media Display Application
 3.1  Enable LCD
 3.2  Background Enable
 3.3  Media Keys
  3.3.1  Winamp
  3.3.2  WinDVD

4.  Logitech G-series LCD Clock
 4.1  Supported Email Clients

5.  Logitech G-series LCD Countdown Timer
 5.1  Timer Labels
 5.2  Timer Modes
 5.3  Timer Controls

6.  Logitech G-series LCD POP3 Monitor
 6.1 Configuration
  6.1.1 POP3 Settings
  6.1.2 Authentication Settings
  6.1.3 Test Connection
  6.1.4 Email Access
  6.1.5 POP3 Email Application Shortcut
 6.2 Operation
  6.2.1 Launch Email Application
  6.2.2 Synchronize
  6.2.3 Cycle Messages
  6.2.4 Mark for Deletion

7.  Logitech G-series LCD Tabs

8.  DEVICE-SPECIFIC NOTES

9.  GAME-SPECIFIC NOTES

10.  TROUBLESHOOTING
 10.1  3RD-PARTY INSTALLATION PROGRAMS

11.  CONTACTING CUSTOMER SUPPORT

****************************************************************************

1. WHAT'S IN THIS RELEASE

1.1 SUPPORTED OPERATING SYSTEMS

Logitech G-series Keyboard Software is designed to work with
the Windows XP operating systems only.

****************************************************************************

2. INSTALLING AND REMOVING THE SOFTWARE UNDER WINDOWS XP

2.1 QUICK INSTALLATION

Windows may be restarted during the installation process. We recommend
saving any work and closing any open programs before beginning the
installation.

It is not necessary to uninstall previous versions of the software
prior to installation. The installer will check for previous versions
and remove or update files as necessary.

To begin the installation place the CD in your CD-ROM drive. The Setup
window appears in a few seconds (if AutoPlay is enabled on your
system), then follow the directions that appear on the setup screens.

If the AutoPlay feature of Windows XP is not enabled, then simply go
to the Start menu, choose "Run", click "Browse" to select the CD-ROM
drive, double-click the "SETUP.EXE" program on the CD, then follow the
directions that appear on the setup screens.

2.2 UNINSTALL

The software can be removed from Windows XP by clicking the Add/Remove
Programs icon in the Windows XP Control Panel. Select "Logitech G-series 
Keyboard Software" and click the "Add/Remove..." button. Then 
follow the on screen instructions for completing the uninstall.

****************************************************************************

3. LCD MEDIA DISPLAY

 To configure the Logitech G-series Media Display add-on:-

 1. Right click the Logitech G-series Keyboard Profiler system tray icon and
 choose LCD Control Panel.
 2. In the Logitech G-series LCD Manager, choose Logitech G-series Media Display 
 from the list, and click the Configure button.

3.1 Enable LCD
 The Enable LCD option is used to determine whether, or not, to display 
 the player's media information on the LCD (supported players only). The
 following media players are supported by this feature:
 - iTunes
 - RealPlayer
 - Sonique
 - Winamp
 - Music Match
 - Windows Media Player (9 and above)
 - MediaLife

3.2 Background Enable
 The Background Enable option allows certain (supported) players to make 
 use of the "media" keys on the keyboard while they are in the background.  
 Unlike the other supported players, these few are not capable of doing this 
 by default.  The following media players are supported by this feature:

 - iTunes 4.7
 - RealPlayer 10
 - Sonique

3.3 Media Keys

3.3.1 Winamp
 In order to enable the "media" keys for use in Winamp, you will need to 
 enable the "Global Hot Keys" option in the Winamp preferences.  Please 
 refer to the Winamp help for details.

3.3.2 WinDVD
 When the Logitech G-series Media Display is running, media keys will work
 for WinDVD when one of its windows is in the foreground. 

****************************************************************************

4.0 Logitech G-series LCD Clock
 This add-on will display current date & time, as well as the total number 
 of unread emails for accounts within the supported email clients.

4.1 Supported Email Clients
 Microsoft Outlook Express
 Microsoft Outlook 2003
 Thunderbird
 MSN Hotmail (only available when MSN Messenger is active.)

****************************************************************************

5.  Logitech G-series LCD Countdown Timer
 When you first install LCDCountdown, it will show default labels and values. 
 To change these, go into the LCD Control Panel, select LCD Countdown Timer, 
 and click on configure.

 The first thing you can do on the configure is decide whether you want to 
 use one timer or two timers.  You can enable or disable Timer 1 or Timer 2 
 by clicking on the checkbox.

5.1 Timer Labels
 To change the timer label, open the configuration, then you can change the 
 label in the edit box under "Label:".

5.2 Timer Modes
 Underneath the timer label is the mode option.

 "Stopwatch" mode will count up like a stopwatch.
 "Countdown" mode requires that you enter a time to countdown from.

 The time input has three boxes which are hours, minutes, and seconds.
 There are two more options you can use.  The first is "Loop", which will 
 restart your timer automatically when the timer reaches zero.  The second is 
 to play a sound when the timer reaches zero.  Click the checkbox to enable 
 a sound, then browse for a sound file with the "..." button.

5.3 Timer Controls
 On the LCD itself, press the triangle to start a timer, the two bars to pause, 
 and the bar/arrow to reset (these are standard symbols).  When paused the 
 pause button will become a start button again.  When the timer reaches zero, 
 and you are not looping, you can only reset the counter.

****************************************************************************

6.  Logitech G-series LCD POP3 Monitor
 Enables you to retrieve and preview the headers of email messages from your 
 favorite POP3 provider.

6.1 Configuration
 Upon being launched for the first time, the applet would need to be configured
 so it can communicate with the POP3 provider of your choosing.  The settings dialog
 can be accessed via the LCD buttons by clicking on the button under the Wrench 
 icon.

 6.1.1 POP3 Settings
  Mail Server - Enter the your POP3 mail server address.
  Port Number - Enter the port number for the POP3 mail server.

 6.1.2 Authentication Settings
  User Name - Enter your user name for your POP3 account.
  Password - Enter the password that you use to access your POP3 account.

 6.1.3 Test Connection
  Once you have setup your POP3 account to be used, you can test your settings 
  by pressing the Test Connection button.

 6.1.4 Email Access
  You can set the interval in which the applet will check for new messages on 
  your POP3 account.

 6.1.5 POP3 Email Application Shortcut
  You can set up the LCD email button to either launch the default email
  application, or one that you specify.  To set up a custom application, 
  click the browse button and choose the executable for the application that 
  would like to use.

6.2 Operation
 6.2.1 Launch Email Application
  The button marked by the picture of the envelope will launch the email 
  application that is selected in the cofiguration settings for the applet.
 6.2.2 Synchronize
  The button marked by the picture of the two arrows will synchronize your 
  POP3 mail account.
 6.2.3 Cycle Messages
  The buttons marked by the pictures of the left and right arrows will 
  navigate to the previous and next messages.
 6.2.4 Mark for Deletion
  The button marked by the X will mark the current message for deletion.

****************************************************************************

7.  Logitech G-series LCD Tabs
 By default the "reserve" button cycles your LCD applets.
 To get to "tabs" mode, press and hold the reserve button for 3 seconds. In
 "Tabs" mode, the reserve button cycles the different configurations tabs.

7.1  Tab Selections
 The different tabs available are:-
 - Switch to Program
 - Switching Algorythm
 - Quick Switch Mode
 - Brightness
 - Contrast
 - Display Time
 
 7.1.1 Switch to Program
  Switches between the LCD applets that are running.

 7.1.2 Switching Algorythm
  - Manual (Use the LCD controls):
    Use the soft buttons to switch applets.
  - Manual (Allow High Priority):
    Same as above, except allows high priority applets to take precedence.
  - Rotate Between Programs
    Cycles through each of the running LCD applets.
  - Rotate Using Klever-VU
    Uses an algorithm to allocate time slots for applets in a 'smart' manner.

 7.1.3 Quick Switch Mode
  Switches to the next applet without having to see the current applet's 
  name first.
  - Disable
  - Enable

 7.1.4 Brightness
  Brightness has three settings: High, Medium, Low

 7.1.5 Contrast
  Contrast has three settings: High, Medium, Low

 7.1.6 Display Time
  Sets how many seconds are given to each applet in rotation mode.  This 
  option can be set between 1-20 seconds.
 
****************************************************************************

8. DEVICE SPECIFIC NOTES

****************************************************************************

9. GAME-SPECIFIC NOTES

****************************************************************************

10. TROUBLESHOOTING

This section describes solutions to specific problems you may have.
Also, you may refer to the Troubleshooting section of the Logitech G-series 
Keyboard Profiler Online Help.

10.1 3RD-PARTY INSTALLATION PROGRAMS

It is suggested that the Windows uninstaller be used rather than 3rd
party uninstall programs. If during the installation of the Logitech G-series 
Keyboard software, there are any conflicts with a monitoring program, quit the
installation, then turn off the monitoring program and re-install the Logitech 
G-series Keyboard software.

****************************************************************************

11. CONTACTING CUSTOMER SUPPORT

Web Site: http://www.logitech.com

The Logitech web site contains interactive customer support, patches,
product information and more.

FTP Site: ftp://ftp.logitech.com

The FTP site contains patches and driver updates for our products.

Electronic Support: http://www.logitech.com/support

Email our professional staff with your customer support questions
through our web based service.

Customer Support Hotline: 
    +1 702-269-3457 (US) (Mon - Fri, 6am - 6pm PST)
    +1 416-207-2782 (CA) (Mon - Fri, 6am - 6pm PST) 
    (Note: Hours are subject to change)

    The Customer Support Group will answer any technical or service
    related questions regarding Logitech products.

----------------------------------------------------------------------------
(c) 2004-2006 Logitech.  All rights reserved.  Logitech, the Logitech logo,
and other Logitech marks are owned by Logitech and may be registered.
All other trademarks are the property of their respective owners.

