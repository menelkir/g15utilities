File LEGGIMI di Software tastiera G-Series Logitech

(c) 2004-2006 Logitech.  Tutti i diritti riservati.  Logitech, il logo 
Logitech e altri marchi Logitech sono di propriet� di Logitech e 
potrebbero essere registrati.
Tutti gli altri marchi appartengono ai rispettivi proprietari.

SOMMARIO FILE LEGGIMI

1.  PANORAMICA
 1.1 SISTEMI OPERATIVI SUPPORTATI

2.  INSTALLAZIONE E RIMOZIONE DEL SOFTWARE
 2.1    INSTALLAZIONE
 2.2    DISINSTALLAZIONE

3.  CONFIGURAZIONE DELL'APPLICAZIONE VISUALIZZAZIONE MULTIMEDIALE 
G-SERIES LOGITECH
 3.1 ATTIVA DISPLAY LCD
 3.2 ATTIVA SFONDO
 3.3  TASTI MEDIA
  3.3.1  WINAMP
  3.3.2  WinDVD

4.  OROLOGIO LCD G-SERIES LOGITECH
 4.1  CLIENT DI POSTA ELETTRONICA SUPPORTATI

5.  TIMER CONTO ALLA ROVESCIA LCD G-SERIES LOGITECH
 5.1  ETICHETTE DEL TIMER
 5.2  MODALIT� DEL TIMER
 5.3  COMANDI DEL TIMER

6.  MONITOR G-SERIES LCD POP3 LOGITECH
 6.1 CONFIGURAZIONE
  6.1.1 IMPOSTAZIONI POP3
  6.1.2 IMPOSTAZIONI DI AUTENTICAZIONE
  6.1.3 VERIFICA DELLA CONNESSIONE
  6.1.4 ACCESSO ALLA POSTA ELETTRONICA
  6.1.5 COLLEGAMENTO APPLICAZIONE DI POSTA ELETTRONICA POP3
 6.2 FUNZIONAMENTO
  6.2.1 LANCIO DELL'APPLICAZIONE DI POSTA ELETTRONICA
  6.2.2 SINCRONIZZAZIONE
  6.2.3 VISUALIZZAZIONE DEI MESSAGGI IN SUCCESSIONE
  6.2.4 CONTRASSEGNO PER ELIMINAZIONE

7.  SCHEDE LCD G-SERIES LOGITECH

8.  CONSIDERAZIONI RELATIVE AI DISPOSITIVI

9.  CONSIDERAZIONI RELATIVE AI GIOCHI

10.  RICERCA E SOLUZIONE DEI PROBLEMI
 10.1    PROGRAMMI DI INSTALLAZIONE DI TERZE PARTI

11.  COME CONTATTARE IL SERVIZIO DI ASSISTENZA CLIENTI

****************************************************************************

1. CONTENUTO DELLA PRESENTE VERSIONE

1.1 SISTEMI OPERATIVI SUPPORTATI

 Il Software tastiera G-Series Logitech � stato realizzato per essere 
 eseguito esclusivamente con il sistema operativo Windows XP.

****************************************************************************

2. INSTALLAZIONE E RIMOZIONE DEL SOFTWARE IN WINDOWS XP

 2.1 INSTALLAZIONE RAPIDA

  Windows potrebbe essere riavviato durante il processo di installazione. 
  Prima di iniziare l'installazione si consiglia di salvare il lavoro in 
  corso e di chiudere i programmi aperti.

  Prima dell'installazione non � necessario disinstallare le versioni 
  precedenti del software. Il programma di installazione verifica 
  l'esistenza di versioni precedenti e rimuove o aggiorna i file se 
  necessario.

  Per iniziare l'installazione, inserire il CD nell'unit� CD-ROM. Dopo 
  qualche secondo viene visualizzata la finestra relativa all'installazione 
  (se nel sistema � attivata la funzione di riproduzione automatica di CD), 
  quindi attenersi alle istruzioni visualizzate sullo schermo.

  Se la funzione di riproduzione automatica del CD di Windows XP non � 
  attivata, scegliere Esegui dal menu Start, fare clic su Sfoglia per 
  selezionare l'unit� CD-ROM, fare doppio clic sul programma SETUP.EXE 
  contenuto nel CD-ROM e attenersi alle istruzioni visualizzate.

 2.2    DISINSTALLAZIONE

  Per rimuovere il software da Windows XP, fare clic sull'icona 
  Installazione applicazioni del Pannello di controllo di Windows XP. 
  Selezionare Software tastiera G-Series Logitech e fare clic sul pulsante 
  Aggiungi/Rimuovi. Per completare la disinstallazione, attenersi alle 
  istruzioni visualizzate sullo schermo.

****************************************************************************

3. DISPLAY MULTIMEDIALE LCD

 Per configurare il componente aggiuntivo Visualizzazione multimediale 
 G-series Logitech:

 1. Fare clic con il pulsante destro del mouse sull'icona Gestione profili 
 tastiera G-Series Logitech sulla barra delle applicazioni e scegliere 
 Pannello di controllo display LCD.
 2. In Gestione LCD G-series Logitech scegliere Visualizzazione multimediale 
 G-series Logitech e fare clic sul pulsante Configura.

 3.1 ATTIVA DISPLAY LCD
  L'opzione Attiva display LCD consente di attivare la visualizzazione 
  delle informazioni del lettore multimediale nel display LCD (solo con 
  lettori supportati). I seguenti lettori multimediali supportano questa 
  funzionalit�:
   - iTunes
   - RealPlayer
   - Sonique
   - Winamp
   - Music Match
   - Windows Media Player (versione 9 e successiva)
   - MediaLife

 3.2 ATTIVA SFONDO
  L'opzione Attiva sfondo consente a determinati lettori supportati di 
  utilizzare i tasti multimediali della tastiera mentre sono in esecuzione 
  in background.  Eccetto alcuni lettori supportati, diversi lettori non sono 
  impostati per eseguire questa funzionalit� in modo predefinito.  I seguenti 
  lettori multimediali supportano questa funzionalit�:

   -  iTunes 4,7
   - RealPlayer 10
   - Sonique

 3.3  TASTI MEDIA

  3.3.1  WINAMP
   Per attivare i tasti multimediali con Winamp, � necessario attivare 
   l'opzione Global Hot Keys nelle preferenze di Winamp.  Per informazioni 
   fare riferimento alla guida in linea di Winamp.

  3.3.2  WinDVD
   Se � in esecuzione il componente aggiuntivo Visualizzazione multimediale 
   G-series Logitech, i tasti media funzionano con WinDVD solo quando una 
   delle sue finestre � aperta in primo piano. 

****************************************************************************

4.0 OROLOGIO LCD G-SERIES LOGITECH
 Questo componente aggiuntivo consente di visualizzare la data e l'ora 
 attuali e il numero totale di messaggi di posta elettronica non letti con 
 i client di posta supportati.

 4.1    CLIENT DI POSTA ELETTRONICA SUPPORTATI
  Microsoft Outlook Express
  Microsoft Outlook 2003
  Thunderbird
  MSN Hotmail (disponibile soltanto quando MSN Messenger � in esecuzione).

****************************************************************************

5.  TIMER CONTO ALLA ROVESCIA LCD G-SERIES LOGITECH
 La prima volta che viene installato, il timer del conto alla rovescia LCD 
 visualizza etichette e valori  predefiniti.  Per modificarli, andare al 
 Pannello di controllo display LCD, selezionare Timer conto alla rovescia 
 LCD e fare clic su Configura.

 La prima cosa da fare � decidere se usare un timer o due.  � possibile 
 attivare o disattivare Timer 1 e Timer 2 selezionando la casella di 
 controllo corrispondente.

 5.1  ETICHETTE DEL TIMER
  Per cambiare l'etichetta del timer, accedere alla configurazione e 
  modificare l'etichetta nella casella di modifica situata sotto Etichetta.

 5.2  MODALIT� DEL TIMER
  Sotto l'etichetta del timer si trova l'opzione per le modalit�.

  La modalit� Cronometro conta i secondi in ordine progressivo proprio come 
  un cronometro.
  In modalit� Conto alla rovescia occorre immettere l'orario da cui iniziare 
  il conto alla rovescia.

  Per immettere l'orario utilizzare gli appositi riquadri per le ore, i 
  minuti e i secondi.
  Sono disponibili due opzioni:  la prima (Ciclico) si utilizza per 
  riavviare il timer ogni volta che raggiunge lo zero,  la seconda per 
  emettere un segnale acustico quando il timer raggiunge lo zero.  
  Selezionare la casella di controllo per attivare il segnale acustico, 
  quindi individuare il file audio desiderato utilizzando il pulsante ...

 5.3  COMANDI DEL TIMER
  Premere il triangolo sul display LCD per avviare il timer, le due barre 
  per arrestarlo e la barra/freccia per reimpostarlo (questi simboli sono 
  standard).  Quando viene premuto, il pulsante di arresto si utilizza per 
  far ripartire il timer.  Se non � stata selezionata l'opzione Ciclico, 
  quando il timer raggiunge lo zero pu� essere solamente reimpostato.

****************************************************************************

6.  MONITOR G-SERIES LCD POP3 LOGITECH
 Consente di scaricare dal provider POP3 preferito e di vedere in anteprima 
 le intestazioni dei messaggi di posta elettronica.

 6.1 CONFIGURAZIONE
  Quando viene lanciata per la prima volta,  l'applet deve essere configurata 
  per poter comunicare col provider POP3 desiderato.  Si pu� accedere alla 
  finestra di dialogo delle impostazioni tramite i pulsanti del display 
  facendo clic sull'icona della chiave inglese.

  6.1.1 IMPOSTAZIONI POP3
   Server di posta (POP3) � Immettere l'indirizzo del server di posta 
   elettronica POP3 desiderato.
   Numero porta � Immettere il numero di porta per il server di posta 
   elettronica POP3.

  6.1.2 IMPOSTAZIONI DI AUTENTICAZIONE
   Nome utente - Immettere il nome utente utilizzato per l'account POP3.
   Password - Immettere la password utilizzata per accedere all'account 
   POP3.

  6.1.3 VERIFICA DELLA CONNESSIONE
   Dopo aver configurato l'account POP3 da utilizzare � possibile verificare 
   le impostazioni premendo il pulsante Verifica connessione.

  6.1.4 ACCESSO ALLA POSTA ELETTRONICA
   � possibile determinare l'intervallo di tempo con cui l'applet verifica 
   se sono arrivati dei messaggi di posta elettronica nuovi nell'account 
   POP3.

  6.1.5 COLLEGAMENTO APPLICAZIONE DI POSTA ELETTRONICA POP3
   Si pu� impostare il pulsante Posta elettronica in modo che lanci 
   l'applicazione di posta elettronica predefinita oppure se ne pu� 
   specificare un'altra.  Per specificare un'applicazione personalizzata, 
   fare clic sul pulsante Sfoglia e scegliere il file eseguibile 
   dell'applicazione desiderata.

  6.2 FUNZIONAMENTO
   6.2.1 LANCIO DELL'APPLICAZIONE DI POSTA ELETTRONICA
    Il pulsante identificato dall'icona di una busta lancia l'applicazione 
    di posta elettronica selezionata nelle impostazioni di configurazione 
    dell'applet.
  6.2.2 SINCRONIZZAZIONE
   Il pulsante identificato dall'icona di due frecce sincronizza l'account 
   di posta elettronica POP3.
  6.2.3 VISUALIZZAZIONE DEI MESSAGGI IN SUCCESSIONE
   Con i pulsanti identificati dall'icona di una freccia sinistra e una 
   freccia destra si pu� navigare al messaggio precedente o a quello 
   successivo.
  6.2.4 CONTRASSEGNO PER ELIMINAZIONE
   Il pulsante identificato da una X contrassegna il messaggi corrente per 
   l'eliminazione.
  

****************************************************************************

7.  SCHEDE LCD G-SERIES LOGITECH
 Per impostazione predefinita, il pulsante circolare di riserva consente di 
 passare da un'applet LCD all'altra.
 Per attivare la modalit� schede, tenere premuto il pulsante di riserva per 
 tre secondi. In modalit� schede, il pulsante di riserva consente di passare 
 da una scheda di configurazione all'altra.

 7.1  SELEZIONE DELLE SCHEDE
  Sono disponibili le seguenti schede:
  - Passa al programma
  - Algoritmo di selezione
  - Modalit� Selezione rapida
  - Luminosit�
  - Contrasto
  - Tempo di visualizzazione
 
  7.1.1 PASSA AL PROGRAMMA
   Passa da un'applet LCD all'altra.

  7.1.2 Algoritmo di selezione
   - Manuale (Utilizza i controlli LCD):
     Utilizzare i tasti specifici per passare da un'applet all'altra.
   - Manuale (Consenti priorit� alta):
     Uguale alla precedente, tranne che consente alle applet a priorit� alta 
     di avere la precedenza.
   - Passa automaticamente da un programma all'altro
     Passa in sequenza da un'applet LCD all'altra.
   - Passa da un programma all'altro utilizzando Klever-VU
     Utilizza un algoritmo per assegnare in modo intelligente dei periodi 
     di tempo alle applet.
 
  7.1.3 MODALIT� SELEZIONE RAPIDA
   Passa all'applet successiva senza dover prima vedere il nome dell'applet 
   corrente.
   - Disattiva
   - Attiva

  7.1.4 LUNIMOSIT�
   Sono disponibili tre livelli di luminosit�: Alto, Medio e Basso

  7.1.5 CONTRASTO
   Sono disponibili tre livelli di contrasto: Alto, Medio e Basso

  7.1.6 TEMPO DI VISUALIZZAZIONE
   Imposta il numero di secondi assegnati a ogni applet in modalit� 
   rotazione.  � possibile impostare un valore compreso fra 1 e 20 secondi.
 
****************************************************************************

8. CONSIDERAZIONI RELATIVE AI DISPOSITIVI

****************************************************************************

9. CONSIDERAZIONI RELATIVE AI GIOCHI

****************************************************************************

10. RICERCA E SOLUZIONE DEI PROBLEMI

 In questa sezione sono riportate le soluzioni a determinati problemi che 
 potrebbero verificarsi.
 � inoltre possibile fare riferimento alla sezione sulla risoluzione dei 
 problemi nella Guida in linea di Gestione profili tastiera G-Series Logitech.

 10.1    PROGRAMMI DI INSTALLAZIONE DI TERZE PARTI

  Si consiglia di utilizzare il programma di disinstallazione di Windows 
  piuttosto che quelli realizzati da terze parti. Se durante l'installazione 
  del Software tastiera G-Series Logitech si verificano conflitti con un 
  programma di controllo, interrompere l'installazione, disattivare il 
  programma di controllo e installare nuovamente il software.

****************************************************************************

11. COME CONTATTARE IL SERVIZIO DI ASSISTENZA CLIENTI

 Sito Web: http://www.logitech.com

 Il sito Web Logitech offre il servizio di assistenza clienti interattivo, 
 correzioni, informazioni sui prodotti e altro.

S ito FTP: ftp://ftp.logitech.com

 Il sito FTP offre correzioni e aggiornamenti per i driver dei prodotti.

 Supporto elettronico: http://www.logitech.com/support

 Se si hanno dei quesiti, contattare il personale tecnico via e-mail tramite 
 il servizio di assistenza basato sul Web.

 Hotline telefonica per assistenza clienti: 
     +1 702-269-3457 (USA) (Lun - Ven, 06.00 - 18.00 ora del Pacifico)
     +1 416-207-2782 (CA) (Lun - Ven, 06.00 - 18.00 ora del Pacifico)
     (Nota: l'orario � soggetto a cambiamenti.)

     Il servizio di assistenza clienti fornir� tutti i tipi di informazioni 
     relative ai prodotti Logitech.

----------------------------------------------------------------------------
(c) 2004-2006 Logitech.  Tutti i diritti riservati.  Logitech, il logo 
Logitech e altri marchi Logitech sono di propriet� di Logitech e potrebbero 
essere registrati.
Tutti gli altri marchi appartengono ai rispettivi proprietari.

