Archivo L�AME del software de teclado Logitech G-series

(c) 2004-2006 Logitech. Reservados todos los derechos. Logitech, el 
logotipo de Logitech y las dem�s marcas de Logitech pertenecen a 
Logitech y pueden estar registradas.
Las dem�s marcas comerciales pertenecen a sus respectivos propietarios.

�NDICE

1.  INTRODUCCI�N
 1.1 SISTEMAS OPERATIVOS COMPATIBLES

2.  INSTALACI�N Y ELIMINACI�N DE SOFTWARE
 2.1    INSTALACI�N
 2.2 DESINSTALACI�N

3.  Configuraci�n de la aplicaci�n de pantalla multimedia de Logitech 
G-series
 3.1  Activaci�n de LCD
 3.2 Activaci�n de fondo
 3.3  Botones multimedia
  3.3.1  Winamp
  3.3.2  WinDVD

4.  Reloj de LCD de Logitech G-series
 4.1 Clientes de correo electr�nico compatibles

5.  Cuenta atr�s de LCD de Logitech G-series
 5.1  Etiquetas de contador
 5.2  Modos de contador
 5.3  Controles de contador

6.  Supervisi�n de POP3 en LCD de Logitech G-series
 6.1 Configuraci�n
  6.1.1 Configuraci�n de POP3
  6.1.2 Configuraci�n de autenticaci�n
  6.1.3 Probar conexi�n
  6.1.4 Acceso a correo electr�nico
  6.1.5 Acceso directo a aplicaci�n de correo electr�nico POP3
 6.2 Uso
  6.2.1 Iniciar aplicaci�n de correo electr�nico
  6.2.2 Sincronizaci�n
  6.2.3 Recorrer mensajes
  6.2.4 Marcar para eliminar

7.  Secciones de LCD de Logitech G-series

8.  NOTAS SOBRE DISPOSITIVOS ESPEC�FICOS

9.  NOTAS SOBRE JUEGOS ESPEC�FICOS

10.  RESOLUCI�N DE PROBLEMAS
 10.1 PROGRAMAS DE INSTALACI�N DE OTROS FABRICANTES

11.  CONTACTO CON EL SERVICIO DE ATENCI�N AL CLIENTE

****************************************************************************

1. ESTA VERSI�N

1.1 SISTEMAS OPERATIVOS COMPATIBLES

 El software de teclado Logitech G-series se ha dise�ado exclusivamente 
 para el sistema operativo Windows XP.

****************************************************************************

2. INSTALACI�N Y ELIMINACI�N DE SOFTWARE EN WINDOWS XP

 2.1 INSTALACI�N R�PIDA

  Es posible que se reinicie Windows durante el proceso de instalaci�n. 
  Recomendamos que guarde su trabajo y que cierre los programas abiertos 
  antes de iniciar la instalaci�n.

  No es necesario desinstalar las versiones anteriores del software antes de 
  la instalaci�n. El programa de instalaci�n buscar� las versiones anteriores 
  y eliminar� o actualizar� los archivos necesarios.

  Para iniciar la instalaci�n, introduzca el CD en la unidad de CDROM del 
  ordenador. Al cabo de unos segundos aparecer�  la ventana de instalaci�n 
  (si en el sistema est� activada la funci�n Reproducci�n autom�tica). Siga 
  las instrucciones indicadas en las sucesivas pantallas del programa de 
  instalaci�n.

  Si la funci�n Reproducci�n autom�tica de Windows XP no est� activada, haga 
  clic en Inicio y elija Ejecutar. A continuaci�n, haga clic en el bot�n 
  Examinar para seleccionar la unidad de CDROM y haga doble clic en el 
  programa SETUP.EXE del CD. Siga las instrucciones indicadas en las 
  sucesivas pantallas del programa de instalaci�n.

 2.2 DESINSTALACI�N

  Para eliminar el software de Windows XP, haga clic en el icono Agregar o 
  quitar programas del Panel de control de Windows XP. Seleccione Software 
  de teclado Logitech G-series y haga clic en el bot�n Agregar o quitar. A 
  continuaci�n, siga las instrucciones en pantalla para completar la 
  desinstalaci�n.

****************************************************************************

3. PANTALLA MULTIMEDIA LCD

 Para configurar el complemento Pantalla multimedia de Logitech G-series:

 1. Haga clic con el bot�n derecho del rat�n en el icono de la bandeja del 
 sistema de Perfilador de teclado Logitech G-series  y elija Panel de 
 control LCD.
 2. En el Administrador de LCD de Logitech G-series, elija en la lista 
 Pantalla multimedia de Logitech G-series y haga clic en el bot�n Configurar.

 3.1  Activaci�n de LCD
  La opci�n Activaci�n de LCD se usa para determinar si debe mostrarse la 
  informaci�n multimedia del reproductor en la pantalla LCD (s�lo 
  reproductores compatibles). Est� disponible para estos reproductores 
  de medios:
  - iTunes
  - RealPlayer
  - Sonique
  - Winamp
  - Music Match
  -Reproductor de Windows Media (9 y posteriores)
  - MediaLife

 3.2 Activaci�n de fondo
  La opci�n Activaci�n de fondo permite que algunos reproductores 
  (compatibles) funcionen con las teclas multimedia del teclado mientras 
  est�n en segundo plano.  Pero en �stos no se trata de una funci�n 
  predeterminada.  Est� disponible para los siguientes reproductores de 
  medios:

  - iTunes 4.7
  - RealPlayer 10
  - Sonique

 3.3  Botones multimedia

  3.3.1  Winamp
   Para activar las teclas multimedia de modo que funcionen con Winamp, 
   debe activarse la opci�n "Global Hotkeys" en las preferencias de Winamp.  
   Consulte la ayuda de Winamp para obtener m�s detalles.

  3.3.2  WinDVD
   Cuando est� activa la pantalla multimedia de logitech G-series, los 
   botones multimedia funcionar�n con WinDVD si una de sus ventanas se 
   encuentra en primer plano. 

****************************************************************************

4.0 Reloj de LCD de Logitech G-series
 Este complemento mostrar� la hora y fecha actuales, as� como el n�mero 
 total de mensajes no le�dos de las cuentas de los clientes de correo 
 electr�nico compatibles.

4.1 Clientes de correo electr�nico compatibles
 Microsoft Outlook Express
 Microsoft Outlook 2003
 Thunderbird
 MSN Hotmail (s�lo disponible cuando MSN Messenger est� activo).

****************************************************************************

5.  Cuenta atr�s de LCD de Logitech G-series
 La primera vez que instale LCDCountdown, se mostrar�n las etiquetas y los 
 valores predeterminados.  Para modificarlos, abra el Panel de LCD, 
 seleccione Cuenta atr�s de LCD de Logitech G-series y haga clic en 
 Configurar.

 Para empezar, puede optar por utilizar uno o dos contadores.  Si quiere 
 activar o desactivar la Cuenta atr�s 1 o la Cuenta atr�s 2, haga clic en 
 la casilla correspondiente.

 5.1  Etiquetas de contador
  Para modificar una etiqueta, abra la configuraci�n y edite el contenido 
  del cuadro bajo "Etiqueta:".

 5.2  Modos de contador
  Bajo la etiqueta de cuenta atr�s se encuentra la opci�n de modo.

  El modo "Cron�metro" inicia una cuenta ascendente.
  Para utilizar el modo "Cuenta atr�s" es preciso indicar un valor de 
  tiempo de inicio de la cuenta.

  Hay tres cuadros de tiempo: uno para horas, otro para minutos y un 
  tercero para segundos.
  Adem�s hay otras dos opciones disponibles:  "Bucle" reinicia 
  autom�ticamente la cuenta atr�s al llegar a cero.  La segunda opci�n hace 
  que se emita un sonido al contar hasta cero.  Para activar un sonido, 
  seleccione la casilla y busque un archivo de sonido haciendo clic en el 
  bot�n "...".
 
 5.3  Controles de contador
  En la pantalla LCD, haga clic en el tri�ngulo para iniciar una cuenta 
  atr�s, en las dos barras para hacer una pausa o en la barra/flecha para 
  restablecer la cuenta atr�s (s�mbolos est�ndar).  Durante una pausa, el 
  bot�n de pausa adopta el s�mbolo de bot�n de reproducci�n.  A menos que 
  se haya seleccionado la opci�n de bucle, al llegar a cero la �nica acci�n 
  posible es restablecer el contador.

****************************************************************************

6.  Supervisi�n de POP3 en LCD de Logitech G-series
 Permite ver y utilizar los encabezados de correo de su proveedor POP3 
 favorito.
 
 6.1 Configuraci�n
  La primera vez que se inicia la aplicaci�n, es preciso configurarla para 
  que pueda comunicarse con el proveedor POP3 deseado.  El cuadro de 
  di�logo requerido se abre mediante el bot�n LCD situado bajo el icono de 
  llave inglesa.

  6.1.1 Configuraci�n de POP3
   Servidor de correo: indique el nombre y la direcci�n de su servidor de 
   correo POP3. N�mero de puerto: indique el n�mero de puerto de su servidor 
   de correo POP3.

  6.1.2 Configuraci�n de autenticaci�n
   Nombre de usuario: indique su nombre de usuario de cuenta POP3.
   Contrase�a: indique la contrase�a que utiliza para acceder a su cuenta 
   POP3.

  6.1.3 Probar conexi�n
   Una vez haya especificado la cuenta POP3 que utilizar, puede comprobar 
   el funcionamiento de la configuraci�n haciendo clic en el bot�n Probar 
   conexi�n.

  6.1.4 Acceso a correo electr�nico
   Puede indicar la frecuencia con la que la aplicaci�n debe comprobar si 
   hay nuevos mensajes en la cuenta POP3.

  6.1.5 Acceso directo a aplicaci�n de correo electr�nico POP3
   Puede configurar el bot�n de correo electr�nico de la pantalla LCD para 
   que inicie la aplicaci�n de correo electr�nico predeterminada u otra de 
   su elecci�n.  Para seleccionar otra aplicaci�n, haga clic en el bot�n � 
   y elija el ejecutable de la aplicaci�n deseada.

 6.2 Uso
  6.2.1 Iniciar aplicaci�n de correo electr�nico
   El bot�n con el icono del sobre inicia la aplicaci�n de correo electr�nico 
   previamente seleccionada en la configuraci�n de la aplicaci�n.
  6.2.2 Sincronizaci�n
   El bot�n con el icono de las dos flechas sincroniza su cuenta de correo 
   POP3.
  6.2.3 Recorrer mensajes
   El bot�n con el icono de las flechas izquierda y derecha permite acceder 
   al mensaje anterior y al siguiente.
  6.2.4 Marcar para eliminar
   El bot�n con el icono X marca el mensaje actual como eliminable.
  

****************************************************************************

7.  Secciones de LCD de Logitech G-series
 De forma predeterminada, el bot�n de "reserva" permite pasar de una 
 aplicaci�n LCD a otra. Para activar el modo de "secciones", mant�ngalo 
 pulsado durante 3 segundos. En este modo, el bot�n de reserva pasa de una 
 secci�n de configuraci�n a otra.

 7.1  Secciones
  Las secciones disponibles son las siguientes:
  - Cambiar a programa
  - Algoritmo de cambio
  -  Modo Cambio r�pido
  - Brillo
  - Contraste
  - Tiempo de visualizaci�n
 
  7.1.1 Cambiar a programa
   Conmuta entre las aplicaciones LCD que est�n abiertas en ese momento.

  7.1.2 Algoritmo de cambio
   - Manual (Usar controles de LCD):
     Use los botones de la pantalla para cambiar de aplicaci�n.
   - Manual (Mostrar programas prioritarios):
     Igual que la anterior, pero en este caso tienen preferencia ciertas 
     aplicaciones.
   - Alternar programas:
     Recorre de una en una las aplicaciones LCD abiertas.
   - Alternar mediante Klever-VU:
     Utiliza un algoritmo para asignar tiempo a aplicaciones de forma 
     'inteligente'.

 7.1.3Modo Cambio r�pido
  Activa la siguiente aplicaci�n sin tener que ver el nombre de la 
  aplicaci�n activa en ese momento.
  - Desactivar
  - Activar

  7.1.4 Brillo
   El par�metro de brillo tienen tres opciones: Alto, Medio y Bajo

  7.1.5 Contraste
   El par�metro de contraste tienen tres opciones: Alto, Medio y Bajo

  7.1.6 Tiempo de visualizaci�n
   Especifica los segundos asignados a cada aplicaci�n en modo de rotaci�n.  
   Puede ser cualquier valor entre 1 y 20 segundos.
 
****************************************************************************

8. NOTAS SOBRE DISPOSITIVOS ESPEC�FICOS

****************************************************************************

9. NOTAS SOBRE JUEGOS ESPEC�FICOS

****************************************************************************

10. RESOLUCI�N DE PROBLEMAS

 Esta secci�n describe soluciones para posibles problemas.
 Adem�s, puede consultar la secci�n Resoluci�n de problemas de la Ayuda en 
 pantalla de Perfilador de teclado Logitech G-series.

 10.1 PROGRAMAS DE INSTALACI�N DE OTROS FABRICANTES

  Se recomienda el uso del desinstalador de Windows y no de programas de 
  desinstalaci�n de otros fabricantes. Si durante la instalaci�n del 
  software de teclado Logitech G-series se produce alg�n conflicto con un 
  programa de supervisi�n, salga de la instalaci�n, cierre el programa de 
  supervisi�n y vuelva a instalar el software de  teclado Logitech G-series.

****************************************************************************

11. CONTACTO CON EL SERVICIO DE ATENCI�N AL CLIENTE

 P�gina Web: http://www.logitech.com

 La p�gina Web de Logitech ofrece atenci�n al cliente de forma interactiva, 
 correcciones parciales (parches), informaci�n sobre productos y muchos otros 
 servicios.

 Sitio FTP: ftp://ftp.logitech.com

 El sitio FTP contiene correcciones parciales y actualizaciones de 
 controladores para todos los productos de Logitech.

Asistencia electr�nica: http://www.logitech.com/support

 Env�e por correo electr�nico cualquier pregunta a los profesionales de 
 Logitech a trav�s de Internet.

 L�nea directa de atenci�n al cliente: 
     +1 702-269-3457 (EE�UU): de lunes a viernes, de 6:00 AM a 6:00 PM Hora 
     del Pac�fico (PST)
     +1 416-207-2782 (Canad�): de lunes a viernes, de 6:00 AM a 6:00 PM Hora 
     del Pac�fico (PST)
     Nota: horarios sujetos a posibles cambios

     El servicio telef�nico de atenci�n al cliente responder� a cualquier 
     pregunta t�cnica relacionada con productos Logitech.

----------------------------------------------------------------------------
(c) 2004-2006 Logitech.  Reservados todos los derechos.  Logitech, el 
logotipo de Logitech y las dem�s marcas de Logitech pertenecen a Logitech y 
pueden estar registradas.
Las dem�s marcas comerciales pertenecen a sus respectivos propietarios.

