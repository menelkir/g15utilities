Tastatursoftware f�r Logitech G-series - README

(c) 2004-2006 Logitech.  Alle Rechte vorbehalten.  Logitech, das 
Logitech-Logo und andere Warenzeichen von Logitech sind Eigentum von 
Logitech und sind zum Teil amtlich eingetragene Marken.
Alle anderen Marken sind Eigentum der jeweiligen Inhaber.

README - INHALT

1.  �bersicht
 1.1    Unterst�tzte Betriebssysteme

2.  Installieren und Entfernen der Software
 2.1    Installieren
 2.2    Deinstallieren

3.  Konfigurieren der Multimediaanzeige-Anwendung f�r die Logitech G-series
 3.1    LCD aktivieren
 3.2    Hintergrund aktivieren
 3.3  Media-Tasten
  3.3.1  Winamp
  3.3.2  WinDVD

4.  LCD-Uhr der Logitech G-series
 4.1    Unterst�tzte E-Mail-Clients

5.  Logitech G-Series LCD - Countdown-Zeitgeber
 5.1  Zeitgeber-Bezeichnung
 5.2  Zeitgeber-Modi
 5.3  Zeitgeber-Bedienelemente

6.  Logitech G-Series LCD - POP3-Monitor
 6.1 Konfiguration
  6.1.1 POP3-Einstellungen
  6.1.2. Authentifizierungseinstellungen
  6.1.3 Verbindung pr�fen
  6.1.4 E-Mail-Zugriff
  6.1.5 POP3-E-Mail-Anwendungsverkn�pfung
 6.2 Betrieb
  6.2.1 E-Mail-Anwendung starten
  6.2.2 Synchronisieren
  6.2.3 Nachrichten durchlaufen
  6.2.4 Zum L�schen markieren

7.  LCD-Registerkarten der Logitech G-series

8.  Ger�tespezifische Hinweise

9.  Hinweise zu Spielen

10.  Fehlerbehebung
 10.1    Installieren von Drittanbieter-Anwendungen

11.  Kundendienst

****************************************************************************

1. Infos zu diesem Release

1.1    Unterst�tzte Betriebssysteme

 Die Tastatursoftware f�r Logitech G-series wird nur von Windows XP 
 unterst�tzt.

****************************************************************************

2. Installieren und Entfernen der Software

2.1    Installieren

 M�glicherweise erfolgt w�hrend der Installation ein Neustart von Windows. Vor 
 der Installation sollten Sie alle offenen Dateien speichern und alle laufenden 
 Anwendungen schlie�en.

 Fr�here Versionen der Software brauchen nicht deinstalliert zu werden. Das 
 Installationsprogramm sucht nach fr�heren Versionen und entfernt bzw. 
 aktualisiert die Dateien entsprechend.

 Legen Sie zum Starten der Installation die CD ein. Wenn die AutoPlay-Funktion 
 auf Ihrem System aktiviert ist, wird das Setup nach kurzer Zeit gestartet. 
 Folgen Sie den Anweisungen auf dem Bildschirm.

 Ist die AutoPlay-Funktion von Windows XP nicht aktiviert, w�hlen Sie im 
 Startmen� den Befehl "Ausf�hren" und klicken auf "Durchsuchen", um das 
 CD-ROM-Laufwerk auszuw�hlen. Doppelklicken Sie auf "SETUP.EXE" und folgen 
 Sie den Anweisungen auf dem Bildschirm.

2.2    Deinstallieren

 Sie k�nnen die Software �ber die Option "Software" in der Windows 
 XP-Systemsteuerung deinstallieren. Markieren Sie "Logitech G-series 
 Tastatursoftware" und klicken Sie auf "Hinzuf�gen/Entfernen...". Folgen Sie 
 dann den Anweisungen auf dem Bildschirm.

****************************************************************************

3. Konfigurieren der Multimediaanzeige der Logitech G-series

 So konfigurieren Sie die Multimediaanzeige der Logitech G-series:

 1. Klicken Sie mit der rechten Maustaste in der Windows-Taskleiste auf das 
 Symbol "Logitech G-series Keyboard Profiler" und w�hlen Sie 
 "LCD-Systemsteuerung".
 2. W�hlen Sie in "Logitech G-series LCD Manager" in der Liste den Eintrag 
 "Logitech G-series  Multimediaanzeige" und klicken Sie auf "Konfigurieren".

3.1    LCD aktivieren
 Mit der Option "LCD aktivieren" bestimmen Sie, ob Multimediainformationen 
 der Wiedergabeanwendung im  LCD angezeigt werden sollen (nur f�r unterst�tzte 
 Wiedergabeanwendungen). Folgende  Wiedergabeanwendungen unterst�tzen diese 
 Funktion:
 - iTunes
 - RealPlayer
 - Sonique
 - Winamp
 - Music Match
 - Windows Media Player (9 und h�her)
 - MediaLife

3.2    Hintergrund aktivieren
 Die Option "Hintergrund aktivieren" erm�glicht den Einsatz der 
 Multimediabedienelemente der Tastatur f�r bestimmte (unterst�tzte) 
 Wiedergabeanwendungen im Hintergrund.  Anders als die anderen unterst�tzen 
 Wiedergabeanwendungen, sind diese standardm��ig nicht dazu in der Lage.  
 Folgende Wiedergabeanwendungen unterst�tzen diese Funktion:

 - iTunes 4.7
 - RealPlayer
 - Sonique

3.3  Media-Tasten

3.3.1  Winamp
 Damit Sie die Multimediabedienelemente der Tastatur in Winamp verwenden 
 k�nnen, m�ssen Sie die Option "Global Hotkeys" in den Winamp-Einstellungen 
 aktivieren.  Weitere Informationen finden Sie in der Winamp-Hilfe.

3.3.2  WinDVD
 Wenn die Logitech G-series Multimediaanzeige l�uft, funktionieren die 
 Media-Tasten f�r WinDVD, vorausgesetzt, eines seiner Fenster befindet sich 
 im Vordergrund. 

****************************************************************************

4.0 LCD-Uhr der Logitech G-series
 Dieses Add-on zeigt das aktuelle Datum und die Uhrzeit sowie die Anzahl der 
 ungelesenen E-Mails f�r Konten in unterst�tzten E-Mail-Clients an.

4.1    Unterst�tzte E-Mail-Clients
 Microsoft Outlook Express
 Microsoft Outlook 2003
 Thunderbird
 MSN Hotmail (nur verf�gbar, wenn MSN Messenger aktiv ist)

****************************************************************************

5.  Logitech G-Series LCD - Countdown-Zeitgeber
 Direkt nach der Installation von LCDCountdown werden die 
 Standardbezeichnungen und �werte angezeigt.  Um diese ggf. zu �ndern, gehen 
 Sie in die LCD-Systemsteuerung, w�hlen "LCD-Countdown-Zeitgeber" und klicken 
 auf "Konfigurieren".

 Als erstes k�nnen Sie hier festlegen, ob Sie einen oder zwei Zeitgeber 
 verwenden wollen.  Sie k�nnen Zeitgeber 1 oder 2 aktivieren oder 
 deaktivieren.

5.1  Zeitgeber-Bezeichnung
 Um die Zeitgeber-Bezeichnung zu �ndern, �ffnen Sie die Konfiguration und 
 �ndern dann die Beschriftung im Textfeld unter "Bezeichnung".

5.2  Zeitgeber-Modi
 Unterhalb der Zeitgeber-Bezeichnung k�nnen Sie den gew�nschten Modus 
 einstellen.

 Im Modus "Stoppuhr" wird wie bei einer Stoppuhr hochgez�hlt.
 F�r "Countdown" m�ssen Sie eine Zeit eingeben, von der aus nach unten gez�hlt 
 werden kann.

 Die Zeitangabe besteht aus drei Feldern: Stunden, Minuten und Sekunden.
 Daneben gibt es noch zwei zus�tzliche Optionen.  Die erste ist "Schleife". 
 Sie startet den Zeitgeber bei Erreichen von Null automatisch.  Die zweite 
 bewirkt das Abspielen eines Tons, wenn der Zeitgeber Null erreicht.  Klicken 
 Sie auf das Kontrollk�stchen, um den Ton zu aktivieren, und suchen Sie dann 
 �ber "..." nach einer geeigneten Audiodatei.

5.3  Zeitgeber-Bedienelemente (auf dem LCD)
 Das Dreieck (Vorw�rtspfeil) startet einen Zeitgeber, die beiden senkrechten 
 Balken legen eine "Pause" ein, Balken/Pfeil dient zum R�cksetzen (dies sind 
 Standardsymbole).  Im Pausenzustand dient die Pausentaste als Starttaste.  
 Wenn der Zeitgeber Null erreicht und Sie nicht im Schleifenmodus sind, ist 
 nur ein R�cksetzen des Zeitgebers m�glich.

****************************************************************************

6.  Logitech G-Series LCD - POP3-Monitor
 Dient zum Auffinden und Anzeigen der Kopfleisten von E-Mail-Nachrichten 
 Ihres bevorzugten POP3-Providers.

6.1 Konfiguration
 Bei erstmaliger Benutzung muss das Applet konfiguriert werden, damit es mit 
 dem von Ihnen gew�hlten POP3-Provider kommunizieren kann.  Das Dialogfeld 
 "Einstellungen" kann �ber die LCD-Tasten aufgerufen werden: Klicken Sie auf 
 die Taste unter dem Schraubenschl�ssel.

 6.1.1 POP3-Einstellungen
  Mail-Server � Geben Sie Adresse Ihres POP3-Mail-Servers ein.
  Portnummer � Geben Sie die Portnummer des POP3-Mail-Servers ein.

 6.1.2. Authentifizierungseinstellungen
  Benutzername � Geben Sie den Benutzernamen f�r Ihr POP3-Konto ein.
  Kennwort � Geben Sie das Kennwort ein, das Sie f�r Ihr POP3-Konto verwenden.

 6.1.3 Verbindung pr�fen
  Wenn Sie das POP3-Konto eingerichtet haben, k�nnen Sie die Einstellungen 
  �ber "Verbindung testen" ausprobieren.

 6.1.4 E-Mail-Zugriff
  Hier k�nnen Sie die H�ufigkeit einstellen, mit der das Applet Ihr 
  POP3-Konto auf Neueing�nge �berpr�ft.

 6.1.5 POP3-E-Mail-Anwendungsverkn�pfung
  Hier k�nnen Sie festlegen, dass die LCD-Email-Taste entweder die 
  Standard-E-Mail- oder eine andere Anwendung Ihrer Wahl startet.  Zur 
  Auswahl einer anderen Anwendung klicken Sie auf "Durchsuchen" und w�hlen 
  die betreffende Programmdatei.

6.2 Betrieb
 6.2.1 E-Mail-Anwendung starten
  Die Taste mit dem Kuvert startet die E-Mail-Anwendung, die unter den 
  Konfigurationseinstellungen des Applet ausgew�hlt wurde.

 6.2.2 Synchronisieren
  Die Taste mit den beiden Pfeilen bewirkt eine Synchronisierung (Abstimmung) 
  Ihres POP3-Mail-Kontos.

 6.2.3 Nachrichten durchlaufen
  Die Tasten mit den Rechts- und Linkspfeilen schalten zur vorherigen und 
  zur n�chsten Nachricht.

 6.2.4 Zum L�schen markieren
  Die Taste mit dem "X" kennzeichnet die aktuelle Nachricht zum L�schen.
  

****************************************************************************

7.  LCD-Registerkarten der Logitech G-series
 Im Standardmodus durchl�uft die Reserve-Taste Ihre LCD-Applets.
 Um in den "Register"-Modus zu schalten halten Sie die Reserve-Taste 3 
 Sekunden lang gedr�ckt. Im "Register"-Modus durchl�uft die Reserve-Taste 
 die verschiedenen Konfigurations-Registerkarten.

7.1  Registerkarten-Auswahl
 Verf�gbare Registerkarten:
 - Umschalten zum Programm
 - Umschaltalgorithmus
 - Schnellumschalt-Modus
 - Helligkeit
 - Kontrast
 - Anzeigezeit
 
 7.1.1 Umschalten zum Programm
  Schaltet um zwischen den laufenden LCD-Applets.

 7.1.2 Umschaltalgorithmus
  - Manuell (�ber die LCD-Elemente):
    Verwenden Sie die Soft-Tasten zum Umschalten.
  - Manuell (hohe Priorit�t zulassen):
    Wie oben, nur haben hier die Applets mit hoher Priorit�t Vorrang.
  - Zwischen Programmen wechseln
    Durchl�uft nacheinander die aktiven LCD-Applets.
  - Wechseln mit Klever-VU
    Verwendet einen Algorithmus f�r die "clevere" Zuweisung von Zeitschlitzen 
  f�r Applets.

 7.1.3 Quick Switch-Modus
  Schaltet zum n�chsten Applet, ohne erst den Namen des aktuellen Applet 
  sehen zu m�ssen.
  - Deaktivieren
  - Aktivieren

 7.1.4 Helligkeit
  Es gibt drei Einstellungen: Hoch, Mittel, Niedrig

 7.1.5 Kontrast
  Es gibt drei Einstellungen: Hoch, Mittel, Niedrig

 7.1.6 Anzeigezeit
  Legt fest, wie viele Sekunden jedem Applet im Wechselmodus zugewiesen sind.  
  Einstellungen zwischen 1 und 20 Sekunden sind m�glich.
 
****************************************************************************

8. Ger�tespezifische Hinweise

****************************************************************************

9. Hinweise zu Spielen

****************************************************************************

10. Fehlerbehebung

 Dieser Abschnitt gibt Hinweise zur L�sung m�glicher Probleme.
 Weitere Informationen erhalten Sie im Abschnitt "Fehleranalyse" der 
 Onlinehilfe zu Logitech G-series Keyboard Profiler.

10.1 Installieren von Drittanbieter-Anwendungen
 
 Wir empfehlen, das Windows-Deinstallationsprogramm und keine 
 Installationsprogramme von anderen  Anbietern zu verwenden. Wenn w�hrend 
 der Installation der Tastatursoftware f�r Logitech G-series Konflikte mit 
 �berwachungsprogrammen auftreten, brechen Sie die Installation ab, 
 deaktivieren Sie die �berwachungsanwendung und starten Sie die Installation 
 erneut.

****************************************************************************

11. Kundendienst

Website: http://www.logitech.com

Die Logitech-Website informiert �ber unseren Kundendienst, �ber Patches, 
 Produkte und vieles mehr.

FTP-Site: ftp://ftp.logitech.com

Auf der FTP-Site stellen wir Patches und Treiber-Updates f�r unsere Produkte 
bereit.

Support per E-Mail: http://www.logitech.com/support

Senden Sie Ihre Fragen per E-Mail �ber unseren Internetservice an unsere 
technischen Spezialisten.

Kundendienst-Hotline: 
    +1 702-269-3457 (USA) (Mo - Fr, 6.00 Uhr � 18.00 Uhr PST)
    +1 416-207-2782 (CA) (Mo - Fr, 6.00 Uhr � 18.00 Uhr PST) 
    (Hinweis: �nderungen vorbehalten)

    Unser Kundendienst steht Ihnen f�r alle technischen und Servicefragen 
    im Zusammenhang mit Logitech-Produkten zur Verf�gung.

----------------------------------------------------------------------------
(c) 2004-2006 Logitech.  Alle Rechte vorbehalten.  Logitech, das 
Logitech-Logo und andere Warenzeichen von Logitech sind Eigentum von Logitech 
und sind zum Teil amtlich eingetragene Marken.
Alle anderen Marken sind Eigentum der jeweiligen Inhaber.

