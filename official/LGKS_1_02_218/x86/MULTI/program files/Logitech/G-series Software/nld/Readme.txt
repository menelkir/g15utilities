LEESMIJ-bestand voor toetsenbordsoftware voor Logitech G-Series

(c) 2004-2006 Logitech.  Alle rechten voorbehouden.  Logitech, het 
Logitech-logo en andere Logitech-merken zijn het eigendom van Logitech 
en kunnen gedeponeerd zijn.
Alle�andere�handelsmerken zijn het eigendom van hun respectieve 
eigenaren.

INHOUDSOPGAVE VAN LEESMIJ

1.  OVERZICHT
 1.1  ONDERSTEUNDE BESTURINGSSYSTEMEN

2.  DE SOFTWARE INSTALLEREN EN VERWIJDEREN
 2.1  INSTALLATIE
 2.2  VERWIJDERING

3.  De toepassing Mediadisplay van Logitech G-Series configureren
 3.1  LCD inschakelen
 3.2  Achtergrond inschakelen
 3.3  Mediatoetsen
  3.3.1  WinAmp
  3.3.2  WinDVD

4.  LCD-klok van Logitech G-Series
 4.1  Ondersteunde e-mailclients

5.  LCD-afteltimer van Logitech G-Series
 5.1  Timerlabels
 5.2  Timermodi
 5.3  Timerknoppen

6.  POP3-monitor op LCD van Logitech G-Series
 6.1 Configuratie
  6.1.1 POP3-instellingen
  6.1.2 Verificatie-instellingen
  6.1.3 Verbinding testen
  6.1.4 E-mailtoegang
  6.1.5 Snelkoppeling naar POP3-emailtoepassing
 6.2 Bediening
  6.2.1 E-mailtoepassing starten
  6.2.2 Synchroniseren
  6.2.3 Door berichten schakelen
  6.2.4 Voor verwijdering markeren

7.  LCD-tabs van Logitech G-Series

8.  APPARAATSPECIFIEKE OPMERKINGEN

9.  GAMESPECIFIEKE OPMERKINGEN

10.  PROBLEMEN OPLOSSEN
 10.1  INSTALLATIEPROGRAMMA'S VAN DERDEN

11.  CONTACT OPNEMEN MET DE KLANTENONDERSTEUNING

****************************************************************************

1. WAT ZIT ER IN DEZE VERSIE

1.1 ONDERSTEUNDE BESTURINGSSYSTEMEN

 De toetsenbordsoftware voor Logitech G-Series is alleen voor werking met het 
 Windows XP-besturingssysteem ontworpen.

****************************************************************************

2. DE SOFTWARE INSTALLEREN EN VERWIJDEREN ONDER WINDOWS XP

2.1 SNELLE INSTALLATIE

 Windows wordt misschien opnieuw gestart tijdens het installatieproces. Het 
 is raadzaam uw werk op te  slaan en geopende programma's te sluiten voordat 
 u met de installatie begint.

 Het is niet noodzakelijk eerdere versies van de software te verwijderen 
 voordat u met de installatie  begint. Het installatieprogramma controleert 
 op eerdere versies en zal bestanden zo nodig verwijderen  of updaten.

 Plaats de cd in uw cd-rom-drive om de installatie te beginnen. Na enkele 
 seconden verschijnt het  venster Set-up (als 'Automatisch afspelen' op uw 
 systeem is ingeschakeld). Volg de instructies die op  de setup-schermen 
 verschijnen.

 Is de functie 'Automatisch afspelen' van Windows XP niet ingeschakeld, ga 
 dan gewoon naar het Start- menu, kies 'Uitvoeren', klik op 'Bladeren' om 
 de cd-rom-drive te selecteren, dubbelklik op het programma SETUP.EXE op de 
 cd, en volg de instructies op de setup-schermen.

2.2 VERWIJDERING

 De software kan uit Windows XP verwijderd worden door op het pictogram 
 Software te klikken in het Configuratiescherm van Windows XP. Selecteer 
 'Toetsenbordsoftware voor Logitech G-Series' en klik op de knop 
 Wijzigen/verwijderen. Volg dan de instructies op het scherm om de 
 verwijdering te voltooien.

****************************************************************************

3. LCD-MEDIADISPLAY

 Zo configureert u de invoegtoepassing Mediadisplay van Logitech G-Series:

 1. Klik met de rechtermuisknop op het pictogram Toetsenbord-profiler van 
 Logitech G-Series in het systeemvak, en kies 'LCD-configuratiescherm'.
 2. In de LCD-beheerder van Logitech G-Series kiest u 'Mediadisplay van 
 Logitech G-Series' in de lijst, en klikt u op de knop Configureren.

 3.1 LCD inschakelen
  De optie LCD inschakelen wordt gebruikt om te bepalen of de 
  media-informatie van de speler al dan niet op de LCD moet worden 
  weergegeven (alleen ondersteunde spelers). De volgende mediaspelers worden 
  door deze functie ondersteund:
   - iTunes
   - RealPlayer
   - Sonique
   - WinAmp
   - MusicMatch
   - Windows Mediaspeler (9 en hoger)
   - MediaLife

 3.2 Achtergrond inschakelen
  Met de optie Achtergrond inschakelen kunnen bepaalde (ondersteunde) 
  spelers gebruik maken van de 'mediatoetsen' op het toetsenbord terwijl 
  ze op de achtergrond staan.  In tegenstelling tot andere ondersteunde 
  spelers kunnen deze spelers dit niet standaard doen.  De volgende 
  mediaspelers worden door deze functie ondersteund:

   - iTunes 4.7
   - RealPlayer 10
   - Sonique

 3.3 Mediatoetsen

  3.3.1 WinAmp
   Als u de 'mediatoetsen' wilt inschakelen voor gebruik in WinAmp, dient 
   u de optie Algemene sneltoetsen in de WinAmp-voorkeuren in te schakelen.  
   Raadpleeg de Help van WinAmp voor meer informatie.

  3.3.2 WinDVD
   Wanneer de toepassing Mediadisplay van Logitech G-Series uitgevoerd 
   wordt, werken de mediatoetsen voor WinDVD wanneer een van de 
   WinDVD-vensters op de voorgrond staat. 

****************************************************************************

4.0 LCD-klok van Logitech G-Series
 Deze invoegtoepassing geeft de huidige datum en tijd weer, evenals het 
 totaalaantal ongelezen e-mails voor accounts binnen de ondersteunde 
 e-mailclients.

 4.1 Ondersteunde e-mailclients
  Microsoft Outlook Express
  Microsoft Outlook 2003
  Thunderbird
  MSN Hotmail (alleen beschikbaar wanneer MSN Messenger actief is.)

****************************************************************************

5.  LCD-afteltimer van Logitech G-Series
 Wanneer u LCDCountdown voor het eerst installeert, worden er standaardlabels 
 en -waarden weergegeven.  Wilt u deze wijzigen, dan gaat u naar het 
 LCD-configuratiescherm, selecteert u 'LCD-afteltimer' en klikt u op 
 'Configureren'.

 Tijdens de configuratie kunt u als eerste bepalen of u ��n timer of twee 
 timers wilt gebruiken.  U kunt 'Timer 1' of 'Timer 2' in- of uitschakelen 
 door op het selectievakje te klikken.

 5.1 Timerlabels
  Als u de timerlabel wilt wijzigen, opent u het configuratiescherm. 
  Vervolgens kunt u de label in het bewerkingsvak onder 'Label:' wijzigen.

 5.2 Timermodi
  Onder de timerlabel staat de modusoptie.

  In de modus Stopwatch wordt er omhoog geteld, zoals een stopwatch doet.
  Voor de modus Countdown moet u een tijd invoeren vanwaar afgeteld moet 
  worden.

  De tijdinvoer heeft drie vakken: voor uren, minuten en seconden.
  Daarnaast zijn er nog twee andere opties.  De eerste is 'Lussen', 
  waarmee uw timer automatisch opnieuw gestart wordt wanneer deze nul 
  bereikt.  Met de tweede kunt u een geluid laten afspelen wanneer de 
  timer nul bereikt.  Klik op het selectievakje om een geluid in te 
  schakelen, en vervolgens op de knop ... om een geluidsbestand te zoeken.

 5.3 Timerknoppen
  Op de LCD zelf drukt u op het driehoekje om een timer te starten, op de 
  twee balken om te pauzeren, en op de balk/pijl om te resetten (dit zijn 
  standaardsymbolen).  Wanneer er gepauzeerd is, functioneert de pauzeknop 
  als startknop.  Wanneer de timer nul bereikt en de lusoptie niet 
  ingeschakeld is, kunt u alleen de teller resetten.

****************************************************************************

6.  POP3-monitor op LCD van Logitech G-Series
 Hiermee kunt u de headers van e-mailberichten via de favoriete POP3-provider 
 ophalen en bekijken.

 6.1 Configuratie
 Wanneer het applet voor het eerst gestart wordt, moet het geconfigureerd 
 worden zodat het met uw gewenste POP3-provider kan communiceren.  Het 
 instellingenvenster kan via de LCD-knoppen opgeroepen worden door op de knop 
 onder het moersleutelpictogram te klikken.

  6.1.1 POP3-instellingen
   Mailserver - Voer het adres van uw POP3-mailserver in.
   Poortnummer - Voer het poortnummer voor de POP3-mailserver in.

  6.1.2 Verificatie-instellingen
   Gebruikersnaam - Voer uw gebruikersnaam voor uw POP3-account in.
   Wachtwoord - Voer het wachtwoord in dat u gebruikt om toegang te krijgen 
   tot uw POP3-account.

  6.1.3 Verbinding testen
  Wanneer u de POP3-account die u wilt gebruiken, hebt ingesteld, kunt u uw 
  instellingen testen door op de knop Verbinding testen te drukken.

  6.1.4 E-mailtoegang
  U kunt instellen hoe vaak het applet uw POP3-account op nieuwe berichten 
  controleert.

  6.1.5 Snelkoppeling naar POP3-emailtoepassing
   U kunt de LCD-emailknop zo instellen dat deze de standaard e-mailtoepassing 
   start, of een toepassing die u specificeert.  Wilt u een aangepaste 
   toepassing instellen, dan klikt u op de bladerknop en kiest u het 
   uitvoerbare bestand voor de toepassing die u wilt gebruiken.

 6.2 Bediening
  6.2.1 E-mailtoepassing starten
   Via de knop met een envelop kunt u de e-mailtoepassing starten die u 
   in de configuratie-instellingen voor het applet geselecteerd hebt.
  6.2.2 Synchroniseren
   Via de knop met twee pijlen kunt u uw POP3-mailaccount synchroniseren.
  6.2.3 Door berichten schakelen
   Via de knoppen met een linker- en rechterpijl kunt u naar het vorige en 
   volgende bericht navigeren.
  6.2.4 Voor verwijdering markeren
   Via de knop met een X kunt u het huidige bericht voor verwijdering 
   markeren.
  

****************************************************************************

7.  LCD-tabs van Logitech G-Series
 De 'reserveknop' schakelt standaard door uw LCD-applets.
 Als u naar 'tabmodus' wilt overschakelen, houdt u de reserveknop 3 seconden 
 ingedrukt. In tabmodus kunt u met de reserveknop door de verschillende 
 configuratietabs schakelen.

 7.1 Tabselecties
  De verschillende beschikbare tabs zijn:
  - Overschakelen naar programma
  - Omschakelingsalgoritme
  - Modus Snelle omschakeling
  - Helderheid
  - Contrast
  - Weergavetijd
 
  7.1.1 Overschakelen naar programma
   Hiermee schakelt u tussen de LCD-applets die uitgevoerd worden.

  7.1.2 Omschakelingsalgoritme
   - Handmatig (gebruik de LCD-knoppen):
     Gebruik de zachte knoppen om tussen applets te schakelen.
   - Handmatig (Hoge prioriteit toestaan):
     Hetzelfde als hierboven, maar applets met hoge prioriteit hebben voorrang.
   - Tussen programma's schakelen
     Hiermee wordt er door alle uitgevoerde LCD-applets geschakeld.
   - Schakelen met behulp van Klever-VU
     Hiermee wordt een algoritme gebruikt om tijdsintervallen voor applets op 
   een 'slimme' manier toe te wijzen.

  7.1.3 Modus Snelle omschakeling
   Hiermee wordt er naar het volgende applet overgeschakeld zonder dat de 
   appletnaam eerst te zien is.
   - Uitschakelen
   - Inschakelen

  7.1.4 Helderheid
   Helderheid heeft drie instellingen: Hoog, Gemiddeld en Laag

  7.1.5 Contrast
   Contrast heeft drie instellingen: Hoog, Gemiddeld en Laag

  7.1.6 Weergavetijd
   Hiermee wordt ingesteld hoeveel seconden elk applet in rotatiemodus 
   blijft staan.  Deze optie kan tussen 1-20 seconden ingesteld worden.
 
****************************************************************************

8. APPARAATSPECIFIEKE OPMERKINGEN

****************************************************************************

9. GAMESPECIFIEKE OPMERKINGEN

****************************************************************************

10. PROBLEMEN OPLOSSEN

 In deze sectie worden oplossingen beschreven voor specifieke problemen 
 die u eventueel ondervindt.
 U kunt ook de sectie Problemen oplossen raadplegen in de online-Help 
 voor de toetsenbord-profiler van de Logitech G-Series.

 10.1 INSTALLATIEPROGRAMMA'S VAN DERDEN

  Het is raadzaam de Windows-uninstaller te gebruiken in plaats van 
  verwijderingsprogramma's van derden. Als er tijdens de installatie van 
  de toetsenbordsoftware voor Logitech G-Series conflicten optreden met 
  een controleprogramma, verlaat u de installatie, zet u het 
  controleprogramma uit en installeert u de toetsenbordsoftware voor 
  Logitech G-Series opnieuw.

****************************************************************************

11. CONTACT OPNEMEN MET DE KLANTENONDERSTEUNING

Website: http://www.logitech.com

De Logitech-website bevat interactieve klantenondersteuning, patches, 
productinformatie en meer.

FTP-site: ftp://ftp.logitech.com

De FTP-site bevat patches en stuurprogramma-updates voor onze producten.

Elektronische ondersteuning: http://www.logitech.com/support

E-mail uw vragen via onze webservice naar ons professionele personeel.

Hotline voor klantenondersteuning: 
    +1 702-269-3457 (VS) (maandag t/m vrijdag, 06.00 - 18.00 uur PST)
    +1 416-207-2782 (CA) (maandag t/m vrijdag, 06.00 - 18.00 uur PST) 
    (N.B.: Uren zijn aan verandering onderhevig)

    De klantenondersteuning geeft antwoord op alle technische of 
    serviceverwante vragen betreffende Logitech-producten.

----------------------------------------------------------------------------
(c) 2004-2006 Logitech.  Alle rechten voorbehouden.  Logitech, het 
Logitech-logo en andere Logitech-merken zijn het eigendom van Logitech en 
kunnen gedeponeerd zijn.
Alle�andere�handelsmerken zijn het eigendom van hun respectieve eigenaren.

