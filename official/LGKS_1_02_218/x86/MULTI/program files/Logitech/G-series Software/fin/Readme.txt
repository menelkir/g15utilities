Logitech G-series -näppäimistöohjelmiston LUEMINUT-tiedosto

(c) 2004-2006 Logitech. Kaikki oikeudet pidätetään. Logitech, 
Logitech-logo ja muut Logitech-tuotemerkit ovat Logitechin omaisuutta 
ja saattavat olla rekisteröityjä.
Kaikki muut tuotemerkit ovat vastaavasti muiden omistajien omistuksessa.

LUEMINUT - SISÄLLYS

1. YLEISKATSAUS
 1.1 TUETUT KÄYTTÖJÄRJESTELMÄT

2. OHJELMISTON ASENNUS JA ASENNUKSEN PURKAMINEN
 2.1 ASENNUS
 2.2 ASENNUKSEN POISTO

3. Logitech G-Series -medianäyttösovelluksen asetukset
 3.1 Nestekidenäytön käyttöönotto
 3.2 Taustaohjauksen käyttöönotto
 3.3 Medianäppäimet
 3.3.1 Winamp
 3.3.2 WinDVD

4. Logitech G-seriesin nestekidenäytön kello
 4.1 Tuetut sähköpostiasiakasohjelmat

5. Logitech G-seriesin nestekidenäytön ajastinlaskuri
 5.1 Ajastimen otsikot
 5.2 Ajastimen tilat
 5.3 Ajastimen ohjaimet

6. Logitech G-seriesin nestekidenäytön POP3-monitori
 6.1 Asetukset
 6.1.1 POP3-asetukset
 6.1.2 Käyttöoikeuksien tarkistusasetukset
 6.1.3 Testiyhteys
 6.1.4 Sähköpostin käyttö
 6.1.5 POP3-sähköpostisovelluksen pikakäynnistys
 6.2 Käyttö
 6.2.1 Sähköpostisovelluksen käynnistäminen
 6.2.2 Synkronointi
 6.2.3 Viesteissä liikkuminen
 6.2.4 Poistettavaksi merkitseminen

7. Logitech G-seriesin nestekidenäytön välilehdet

8. LAITEKOHTAISIA HUOMAUTUKSIA

9. PELIKOHTAISIA HUOMAUTUKSIA

10. VIANETSINTÄ
 10.1 MUIDEN VALMISTAJIEN ASENNUSOHJELMAT

11. YHTEYDEN OTTAMINEN ASIAKASTUKEEN

****************************************************************************

1. TÄMÄN JULKAISUN SISÄLTÖ

 1.1 TUETUT KÄYTTÖJÄRJESTELMÄT

 Logitech G-series -näppäimistöohjelmisto on tarkoitettu käytettäväksi vain 
 Windows XP -käyttöjärjestelmässä.

****************************************************************************

2. OHJELMISTON ASENNUS JA ASENNUKSEN PURKAMINEN WINDOWS XP 
-KÄYTTÖJÄRJESTELMÄSSÄ

 2.1 PIKA-ASENNUS

 Windows saattaa täytyä käynnistää uudelleen asennuksen aikana. Ennen 
 asennuksen aloittamista sinun kannattaa tallentaa kaikki työt ja lopettaa 
 kaikki ohjelmat.

 Ennen asennusta ei tarvitse poistaa ohjelmiston aiempien versioiden asennusta. 
 Asennusohjelma tarkistaa aiempien versioiden olemassaolon ja poistaa tai 
 päivittää tiedostot tarpeen mukaan.

 Aloita asennus asettamalla CD-levy CD-ROM-asemaan. Asennusikkuna tulee 
 näyttöön muutaman sekunnin kuluttua (jos automaattinen CD-levyn käynnistys on 
 käytössä tietokoneessasi). Noudata sen jälkeen asennusnäytöissä olevia ohjeita.

 Jos Windows XP:n automaattinen käynnistystoiminto ei ole käytössä, avaa 
 Käynnistä-valikko, valitse "Suorita" ja napsauta "Selaa". Valitse CD-ROM-asema, 
 kaksoisnapsauta CD-levyllä olevaa "SETUP.EXE"-ohjelmaa ja noudata 
 asennusnäyttöihin tulevia ohjeita.

2.2 ASENNUKSEN POISTO

 Voit poistaa ohjelmiston Windows XP:stä napsauttamalla Windows XP:n 
 Ohjauspaneelin Lisää tai poista sovellus -kuvaketta. Valitse Logitech 
 G-series -näppäimistöohjelmisto ja napsauta Muuta tai poista -painiketta. 
 Pura asennus noudattamalla näyttöön tulevia ohjeita.

****************************************************************************

3. MEDIANÄYTTÖ

 Logitech G-seriesin medianäytön asetusten määrittäminen:

 1. Napsauta Logitech G-series Keyboard Profiler -kuvaketta tehtäväpalkissa 
 ja valitse LCD-ohjauspaneeli.
 2. Valitse Logitech G-seriesin nestekidenäytön hallintaohjelman luettelosta 
 Logitech G-seriesin medianäyttö ja napsauta Asetukset-painiketta.

3.1 Nestekidenäytön käyttöönotto
 Nestekidenäytön käyttöönottoasetuksella määritellään, näytetäänkö 
 mediasoittimen tiedot näytössä (vain tuetut soittimet). Tämä ominaisuus 
 tukee seuraavia mediasoittimia:
 - iTunes
 - RealPlayer
 - Sonique
 - Winamp
 - Music Match
 - Windows Media Player (9 ja uudemmat)
 - MediaLife

3.2 Taustaohjauksen käyttöönotto
 Kun taustaohjaus on otettu käyttöön, tiettyjä (tuettuja) soittimia voi 
 ohjata näppäimistön mediapainikkeilla silloinkin, kun ne ovat taustalla. 
 Muissa tuetuissa soittimissa tämä toiminto on käytössä oletusasetuksena. Tämä 
 ominaisuus tukee seuraavia mediasoittimia:

 - iTunes 4.7
 - RealPlayer 10
 - Sonique

3.3 Medianäppäimet

3.3.1 Winamp
 Jotta voisit käyttää mediapainikeita Winampin kanssa, sinun täytyy ottaa 
 "Global Hot Keys" -asetus käyttöön Winampin asetuksissa. Katso lisätietoja 
 Winampin ohjeesta.

3.3.2 WinDVD
 Logitech G-seriesin medianäytön ollessa käynnissä medianäppäimet ohjaavat 
 WinDVD-ohjelmaa silloin, kun jokin sen ikkunoista on edustalla. 

****************************************************************************

4. Logitech G-seriesin nestekidenäytön kello
 Tämä ominaisuus näyttää kulloisenkin kellonajan ja päivämäärän sekä 
 tuetuissa sähköpostiasiakasohjelmissa lukemista odottavien viestien 
 yhteismäärän.

4.1 Tuetut sähköpostiasiakasohjelmat
 Microsoft Outlook Express
 Microsoft Outlook 2003
 Thunderbird
 MSN Hotmail (käytettävissä ainoastaan silloin, kun MSN Messenger on 
 aktiivinen)

****************************************************************************

5. Logitech G-seriesin nestekidenäytön ajastinlaskuri
 Kun LCDCountdown asennetaan ensi kertaa, näkyviin tulevat otsikot ja arvot. 
 Jos haluat muuttaa näitä, avaa LCD-ohjauspaneeli, valitse nestekidenäytön 
 ajastinlaskuri ja napsauta kohtaa asetukset.

 Asetuksissa voit ensimmäiseksi määrittää, haluatko käyttää yhtä vai kahta 
 ajastinta. Voit ottaa käyttöön tai poistaa käytöstä Ajastimet 1 ja 2 
 lisäämällä valintamerkit  ruutuihin.

5.1 Ajastimen otsikot
 Voit muuttaa ajastimen otsikon avaamalla asetukset ja muuttamalla otsikon 
 kohdan "Otsikko:" muokkausruudussa.

5.2 Ajastimen tilat
 Ajastimen otsikon alla näkyy tila-asetus.

 "Sekuntikello"-tilassa kello laskee ylöspäin sekuntikellon tavoin.
 "Ajastus"-tila vaatii, että annat ajastimelle ajan, josta se lähtee 
 laskemaan kohti nollaa.

 Aika annetaan kolmeen ruutuun, jotka edustavat tunteja, minuutteja ja 
 sekunteja.
 Käytettävissä on näiden lisäksi kaksi muuta asetusta. Näistä ensimmäinen on 
 silmukka, joka käynnistää ajastimen automaattisesti uudelleen, kun se 
 saavuttaa nollan. Toinen asetus antaa merkkiäänen, kun ajastin saavuttaa 
 nollan. Lisää ruutuun valintamerkki, jos haluat kuulla merkkiäänen, ja etsi 
 sen jälkeen äänitiedosto selaamalla "..."-painikkeella.

5.3 Ajastimen ohjaimet
 Paina nestekidenäytössä kolmiota käynnistääksesi ajastimen, kahta palkkia 
 keskeyttääksesi sen ja palkkia/nuolta asettaaksesi ajastimen lukeman 
 uudelleen (nämä ovat vakiokuvakkeita). Kun ajastin keskeytetään, 
 keskeytyspainike muuttuu jälleen käynnistyspainikkeeksi. Kun ajastin 
 saavuttaa nollan eikä silmukointi ole käytössä, ainoa vaihtoehto on 
 ajastimen lukeman määrittäminen uudelleen.

****************************************************************************

6. Logitech G-seriesin nestekidenäytön POP3-monitori
 Mahdollistaa sähköpostiviestien otsikoiden noutamisen 
 POP3-palveluntarjoajalta ja niiden esikatselun.

6.1 Asetukset
 Kun sovelma käynnistetään ensimmäistä kertaa, sen asetukset on määritettävä 
 niin, että se pystyy viestimään valitsemasi POP3-palveluntarjoajan kanssa. 
 Pääset asetusikkunaan LCD-painikkeiden avulla napsauttamalla 
 jakoavainkuvakkeen alla olevaa painiketta.

 6.1.1 POP3-asetukset
 Postipalvelin - Anna POP3-postipalvelimen osoite.
 Portin numero - Anna POP3-postipalvelimen portin numero.

 6.1.2 Käyttöoikeuksien tarkistusasetukset
 Käyttäjänimi - Anna POP3-tilisi tunnistama käyttäjänimi.
 Salasana - Anna POP3-tilisi tunnistama salasana.

 6.1.3 Testiyhteys
 Kun olet määrittänyt käytettävän POP3-tilin, voit testata antamasi asetukset 
 painamalla Testiyhteys-painiketta.

 6.1.4 Sähköpostin käyttö
 Voit määrittää, kuinka usein sovelma etsii uusia viestejä POP3-tililtäsi.

 6.1.5 POP3-sähköpostisovelluksen pikakäynnistys
 Voit määrittää nestekidenäytön sähköpostipainikkeen käynnistämään joko 
 oletussähköpostisovelluksen tai erikseen määrittämäsi ohjelman. Määritä 
 sovellus napsauttamalla selauspainiketta ja valitsemalla haluamasi 
 sovelluksen suoritettava tiedosto.

6.2 Käyttö
 6.2.1 Sähköpostisovelluksen käynnistäminen
 Kirjekuoren kuvalla merkitty painike käynnistää sovelman asetuksissa 
 valitun sähköpostisovelluksen.

 6.2.2 Synkronointi
 Kahdella nuolella merkitty painike synkronoi POP3-postitilisi.

 6.2.3 Viesteissä liikkuminen
 Vasemmalle ja oikealle osoittavilla nuolilla merkityt painikkeet siirtävät 
 sinut edelliseen ja seuraavaan viestiin.

 6.2.4 Poistettavaksi merkitseminen
 X-kirjaimella merkitty painike merkitsee senhetkisen viestin poistettavaksi.
  

****************************************************************************

7. Logitech G-seriesin nestekidenäytön välilehdet
 Oletus on, että "varapainike" kierrättää LCD-sovelmia.
 "Välilehtitilaan" pääset painamalla varapainikkeen pohjaan kolmen sekunnin 
 ajaksi. "Välilehtitilassa" varapainike kierrättää eri asetusvälilehtiä.

7.1 Välilehtivalinnat
 Valittavissa ovat seuraavat välilehdet:
 - Vaihto ohjelmaan
 - Vaihtoalgoritmi
 - Ohjelman pikavaihtotila
 - Kirkkaus
 - Kontrasti
 - Näyttöaika
 
 7.1.1 Vaihto ohjelmaan
 Vaihtaa käynnissä olevien LCD-sovelmien välillä.

 7.1.2 Vaihtoalgoritmi
 - Manuaalinen (käytä nestekidenäytön säätimiä):
    Vaihda sovelmia pehmeillä painikkeilla.
 - Manuaalinen (salli etusijalla olevat kohteet):
    Sama kuin yllä, mutta sallii etusijalla olevien sovelmien ohittaa muut.
 - Kierto eri ohjelmien välillä
    Kierrättää käynnissä olevia LCD-sovelmia.
 - Kierto Klever-VU:ta käyttäen
    Jakaa aikajaksoja sovelmille 'älykkäästi' algoritmia käyttäen.

 7.1.3 Ohjelman pikavaihtotila
 Vaihtaa seuraavaan sovelmaan näyttämättä ensin senhetkisen sovelman nimeä.
 - Poista käytöstä
 - Ota käyttöön

 7.1.4 Kirkkaus
 Kirkkausasetuksia on kolme: suuri, keskisuuri ja pieni

 7.1.5 Kontrasti
 Kontrastiasetuksia on kolme: suuri, keskisuuri ja pieni

 7.1.6 Näyttöaika
 Määrittää, kuinka monta sekuntia kukin sovelma näkyy näytössä kiertotilassa. 
 Asetuksen arvo voi olla 1–20 sekuntia.
 
****************************************************************************

8. LAITEKOHTAISIA HUOMAUTUKSIA

****************************************************************************

9. PELIKOHTAISIA HUOMAUTUKSIA

****************************************************************************

10. VIANETSINTÄ

 Tässä osassa on kuvattu ratkaisut tiettyihin ongelmiin, joihin voit törmätä.
 Saat lisätietoja myös Logitech G-series Keyboard Profiler -ohjelmiston 
 käytönaikaisesta ohjeesta.

10.1 MUIDEN VALMISTAJIEN ASENNUSOHJELMAT

  Käytä Windowsiin kuuluva asennuksen poisto-ohjelmaa muiden valmistajien 
  poisto-ohjelmien sijasta. Jos Logitech G-series -näppäimistöohjelmiston 
  asennuksen aikana ilmenee ristiriita asennusta valvovan ohjelman kanssa, 
  lopeta asennus, lopeta asennusta valvova ohjelma ja asenna Logitech 
  G-series -näppäimistöohjelmisto uudelleen.

****************************************************************************

11. YHTEYDEN OTTAMINEN ASIAKASTUKEEN

Web-sivusto: http://www.logitech.com

Logitechin web-sivustossa on saatavissa vuorovaikutteista asiakastukea, 
korjauksia, tuotetietoja ja muita tietoja.

FTP-palvelin: ftp://ftp.logitech.com

FTP-palvelimessa on saatavissa tuotteisiimme liittyviä korjaustiedostoja ja 
ohjainpäivityksiä.

Elektroninen tuki: http://www.logitech.com/support

Voit lähettää kysymyksesi sähköpostina tukihenkilöstöllemme web-pohjaisen 
palvelun kautta.

Asiakastuen puhelin: 
    +1 702 269 3457 (USA) (ma - pe, 16.00 - 04.00 Suomen aikaa)
    +1 416 207 2782 (CA) (ma - pe, 16.00 - 04.00 Suomen aikaa)
    (Huomautus: Aukioloaikoja voidaan muuttaa ilman ennakkoilmoitusta.)

    Asiakastuki vastaa kaikkiin Logitech-tuotteiden tekniikkaan tai 
    palveluihin liittyviin kysymyksiin.

----------------------------------------------------------------------------
(c) 2004-2006 Logitech. Kaikki oikeudet pidätetään. Logitech, Logitech-logo 
ja muut Logitech-tuotemerkit ovat Logitechin omaisuutta ja saattavat olla 
rekisteröityjä.
Kaikki muut tuotemerkit ovat vastaavasti muiden omistajien omistuksessa.

