Программное обеспечение для клавиатуры Logitech G Series - файл README

(c) Logitech, 2004-2006. Все права защищены. Logitech, эмблема Logitech,
и другие товарные знаки Logitech являются собственностью компании 
Logitech и могут быть зарегистрированными товарными знаками.
Все остальные товарные знаки являются собственностью их владельцев.

Файл «README»: ОГЛАВЛЕНИЕ

1. ОБЗОР
 1.1. ПОДДЕРЖИВАЕМЫЕ ОПЕРАЦИОННЫЕ СИСТЕМЫ

2. УСТАНОВКА И УДАЛЕНИЕ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ 
 2.1. УСТАНОВКА
 2.2. УДАЛЕНИЕ

3. Настройка мультимедийного модуля Logitech G Series Media Display
 3.1. Включение ЖК-экрана
 3.2. Включение фона
 3.3. Клавиши Media
 3.3.1. Приложение Winamp
 3.3.2. Приложение WinDVD

4. Часы ЖК-экрана Logitech G Series
 4.1. Поддержка клиентов электронной почты 

5. Таймер ЖК-экрана Logitech G Series
 5.1. Ярлыки таймера
 5.2. Режимы таймера
 5.3. Элементы управления таймера

6. POP3-монитор ЖК-экрана Logitech G Series
 6.1. Конфигурация
 6.1.1. Настройки POP3-монитора
 6.1.2. Настройки проверки подлинности
 6.1.3. Тестовое подключение
 6.1.4. Доступ к электронной почте
 6.1.5. Ярлык приложения электронных сообщений POP3
 6.2. Работа приложения
 6.2.1. Запуск приложения электронных сообщений
 6.2.2. Синхронизация
 6.2.3. Просмотр сообщений
 6.2.4. Удаление сообщений

7. Вкладки ЖК-экрана Logitech G Series

8. СВЕДЕНИЯ ОБ УСТРОЙСТВАХ

9. СВЕДЕНИЯ ОБ ИГРАХ

10. Устранение неполадок 
 10.1. ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ НЕЗАВИСИМЫХ ПРОИЗВОДИТЕЛЕЙ 

11. СВЯЗЬ СО СЛУЖБОЙ ТЕХНИЧЕСКОЙ ПОДДЕРЖКИ

****************************************************************************

1. В ЭТОЙ ВЕРСИИ ПРОДУКТА

 1.1. ПОДДЕРЖИВАЕМЫЕ ОПЕРАЦИОННЫЕ СИСТЕМЫ

  Программное обеспечение клавиатуры Logitech G Series рассчитано на работу
  только под управлением операционной системы Windows XP.

****************************************************************************

2. УСТАНОВКА И УДАЛЕНИЕ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ В ОПЕРАЦИОННОЙ СИСТЕМЕ 
WINDOWS XP

 2.1. БЫСТРАЯ УСТАНОВКА

  Во время установки может потребоваться перезагрузка операционной системы 
  Windows. Рекомендуется сохранить работу и закрыть все приложения перед 
  началом установки.

  Удалять предыдущие версии программного обеспечения
  перед установкой необязательно. Программа установки выполнить проверку на 
  наличие установленных предыдущих версий программного обеспечения, а затем 
  удалит или обновит файлы по мере необходимости.

  Чтобы приступить к установке программного обеспечения, вставьте 
  установочный диск в дисковод CD-ROM. Через несколько секунда появится
  окно мастера установки (если включена функция AutoPlay). Для выполнения 
  установки следуйте инструкциям, которые появляются на экране.

  Если функция AutoPlay не включена,
  выберите в меню «Пуск» команду «Выполнить», а затем нажмите кнопку «Обзор», 
  чтобы выбрать дисковод CD-ROM. Дважды щелкните значок программы «SETUP.EXE» 
  в корневом каталоге установочного диска, а затемследуйте инструкциям, 
  которые появляются на экране.

 2.2. УДАЛЕНИЕ

  Чтобы удалить программное обеспечение в операционной системе Windows XP, 
  щелкните значок «Установка и удаление программ» на панели управления 
  Windows XP. Затем щелкните значок «Программное обеспечение клавиатуры 
  Logitech G Series», а затем нажмите кнопку «Изменить/Удалить...». Далее
  следуйте инструкциям, которые появляются на экране, чтобы выполнить 
  удаление.

****************************************************************************

3. Мультимедийное приложение ЖК-экрана

 Чтобы настроить дополнительный мультимедийный модуль ЖК-экрана Logitech 
 G Series Media Display, выполните следующие действия:

 1. Щелкните правой кнопкой мыши значок клавиатуры Logitech G Series Keyboard 
 Profiler на системной панели, а затем выберите пункт «Панель управления 
 ЖК-экраном».
 2. В окне диспетчера ЖК-экрана Logitech G Series выберите в списке 
 мультимедийный модуль Logitech G Series Media Display и нажмите кнопку 
 «Настройка».

 3.1. Включение ЖК-экрана
  Функция «Включение ЖК-экрана» используется для вывода сведений
  о мультимедиа-проигрывателях на экран ЖК-экрана (выводятся сведения только 
  о поддерживаемых проигрывателях). Эта функция поддерживает следующие 
  проигрыватели:
  - iTunes
  - RealPlayer
  - Sonique
  - Winamp
  - Music Match
  - Windows Media Player (9 и выше)
  - MediaLife

 3.2. Включение фона 
  Функция «Включение фона» позволяет поддерживаемым проигрывателям
  использовать медиа-клавиши на клавиатуре, когда они работают в фоновом 
  режиме. В отличие от других поддерживаемых проигрывателей, в следующих 
  моделях эта функция не включена по умолчанию. Эта функция поддерживает 
  следующие проигрыватели:

  - iTunes 4.7
  - RealPlayer 10
 - Sonique

 3.3. Клавиши Media

  3.3.1. Winamp
   Чтобы использовать медиа-клавиши для работы с проигрывателем Winamp, 
   необходимо включить функцию «Global Hot Keys» в предпочтениях Winamp. 
   Дополнительные сведения см. в справке по Winamp.

  3.3.2. WinDVD
   Если мультимедийный модуль Logitech G Series Media Display работает, 
   медиа-клавиши можно использовать для проигрывателя WinDVD, только если 
   одно из его окон является активным. 

****************************************************************************

4.0. Часы ЖК-экрана Logitech G Series
 Эта надстройка позволяет выводить на экран ЖК-экрана текущую дату и время, 
 а также количество непрочитанных сообщений электронной почты в папках 
 «Входящие» поддерживаемых почтовых клиентов.

 4.1. Поддерживаемые почтовых клиентов 
  Microsoft Outlook Express;
  Microsoft Outlook 2003
  Thunderbird
  MSN Hotmail (только если включен MSN Messenger.)

****************************************************************************

5. Таймер ЖК-экрана Logitech G Series
 При первой установке таймера ЖК-экрана на нем будут отображаться 
 стандартные ярлыки и значения. Чтобы изменить их, выберите на панели 
 управления ЖК-экраном «Таймер ЖК-экрана», а затем щелкните настройки.

 Прежде всего, следует определить, сколько таймеров необходимо использовать - 
 один или два. Таймер 1 и таймер 2 можно включить или выключить, 
 установив соответствующие флажки.

 5.1. Ярлыки таймера
  Чтобы изменить ярлык таймера, откройте настройки, а затем
  измените ярлык в соответствующем поле группы «Ярлык».

 5.2. Режимы таймера
  Под ярлыком таймера расположена функция режима таймера.

  Режим «Секундомер» выполняет отсчет как секундомер.
  Чтобы использовать таймер в режиме «Обратный отсчет», необходимо ввести 
  начальную точку отсчета.

  Для ввода времени начальной точки отсчета предусмотрены три поля: часы, 
  минуты, секунды.В режиме обратного отсчета есть два варианта. Первый вариант 
  - «Цикл» - позволяет повторно запускать обратный отсчет, когда таймер 
  достигает нуля. Второй вариант позволяет по окончании отсчета проигрывать 
  звуковой сигнал. Чтобы использовать второй вариант,выберите звук, который 
  необходимо проигрывать по окончании отсчета. Для этого нажмите кнопку 
  «...» и выберите необходимый звуковой файл.

 5.3. Элементы управления таймера
  На экране ЖК-экрана нажмите кнопку с изображением треугольника, чтобы 
  запустить таймер, кнопку с изображением двух вертикальных столбиков, 
  чтобы приостановить таймер, и кнопку с изображением столбика/стрелки, 
  чтобы сбросить значения (это стандартные обозначения). Чтобы возобновить 
  приостановленный отсчет, нажмите повторно кнопку паузы. По окончании 
  отсчета, если не выбран вариант «Цикл», можно выполнить повторную 
  настройку таймера.

****************************************************************************

6. POP3-монитор ЖК-экрана Logitech G Series
 Эта функция позволяет извлекать и просматривать заголовки (темы) сообщений 
 электронной почты избранного POP3-провайдера.

 6.1. Конфигурация
  При первом запуске необходимо выполнить настройку служебной программы,
  чтобы обеспечить ее взаимодействие с POP3-сервером по выбору пользователя. 
  Диалоговое окно настройки можно открыть, щелкнув одну из кнопок ЖК-экрана, 
  расположенных под значком с изображением гаечного ключа.

  6.1.1. Настройки POP3-монитора
   Почтовый сервер - введите адрес почтового POP3-сервера.
   Номер порта - введите номер порта почтового POP3-сервера.

  6.1.2. Настройки проверки подлинности
   Имя пользователя - введите имя пользователя учетной записи POP3.
   Пароль - введите пароль, использующийся для доступа к учетной записи POP3.

  6.1.3. Тестовое подключение
   После завершения настройки учетной записи POP3, можно проверить работу 
   монитора,нажав кнопку «Тестовое подключение».

  6.1.4. Доступ к электронной почте
   Можно установить интервал, с которым будет осуществляться проверка новых 
   сообщений электронной почты,доставленных на адрес учетной записи POP3.

   Ярлык приложения электронных сообщений POP3
   Можно назначить для кнопки электронной почты ЖК-экрана функцию запуска 
   приложения электронных сообщений,или любого другого приложения по выбору 
   пользователя. Чтобы назначить для клавиши запуск другого приложения по 
   выбору пользователя, нажмите кнопку обзора и выберите исполняющую 
   программу необходимого приложения.

 6.2. Работа приложения
  6.2.1. Запуск приложения электронных сообщений
   Кнопка с изображением почтового конверта используется для запуска 
   приложения электронных сообщений,которое определено в настройках 
   конфигурации служебной программы.
  6.2.2 Synchronize
   Кнопка с изображением двух стрелок используется для выполнения 
   синхронизации пользовательской учетной записи почтового POP3-сервера.
  6.2.3. Просмотр сообщений
   Кнопки с изображением стрелки влево и стрелки вправо используются для
   перехода от одного сообщения к другому.
  6.2.4. Удаление сообщений
   Кнопка с изображением «X» используется для того, чтобы пометить текущее 
   сообщение для последующего удаления.
 

****************************************************************************

7. Вкладки ЖК-экрана Logitech G Series 
 По умолчанию «запасная» кнопка позволяет переключаться между служебными 
 программами ЖК-экрана.Чтобы перейти в «режим вкладок», нажмите и 
 удерживайте запасную кнопку в течение 3 секунд. В режиме вкладок, запасная 
 кнопка позволяет переходить от одной вкладки конфигурации к другой.

 7.1. Выбор вкладок
  Доступны следующие вкладки:
  - Переключение между программами
  - Алгоритм переключения
  Режим быстрого переключения:
  - Яркость
  - Контрастность
  - Время отображения
 
  7.1.1. Переключение между программами 
   Позволяет переключаться между открытыми служебными программами ЖК-экрана.

  7.1.2 Алгоритм переключения 
  - Вручную (с помощью элементов управления ЖК-экраном):
  Переключение между служебными программами с помощью программируемых кнопок.
  - Вручную (Установить высокий приоритет):
  То же, что и предыдущая функция, за тем исключением, что высокоприоритетные 
  приложения имеют преимущество.Чередование программ Переключение между 
  открытыми служебными программами ЖК-экрана.Чередование с использованием 
  технологии Klever-VU Для размещения служебных программ во временных 
  интервалах используется «умный» алгоритм.

  7.1.3. Режим быстрого переключения
   Переключение к следующей служебной программе без предварительного 
   просмотра имени этой программs.
   Отключить
   Включить

  7.1.4. Яркость
   Для параметра «Яркость» предусмотрены три положения переключателя: 
   «Высокая», «Средняя», «Низкая»

  7.1.5. Контрастность
   Для параметра «Контрастность» предусмотрены три положения переключателя: 
   «Высокая», «Средняя», «Низкая» 

  7.1.6. Время отображения 
   Этот параметр позволяет установить длительность задержки для каждой 
   служебной программы в режиме переключения. Можно установить значения в 
   интервале от 1 до 20 секунд.
 
****************************************************************************

8. СВЕДЕНИЯ ОБ УСТРОЙСТВАХ

****************************************************************************

9. СВЕДЕНИЯ ОБ ИГРАХ

****************************************************************************

10. Устранение неполадок 

 В этом разделе приводятся варианты решения типовых проблем.
 Также можно обратиться к разделу «Устранение неполадок» интерактивной 
 справки программы Profiler для клавиатуры Logitech G Series.

 10.1. ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ НЕЗАВИСИМЫХ ПРОИЗВОДИТЕЛЕЙ

  Для удаления программного обеспечения рекомендуется использовать 
  служебные программы Windows, а не программы установки независимых 
  производителей. Если во время установки программного обеспечения для 
  клавиатуры Logitech G Series возникают конфликты с диспетчером, завершите 
  работу установочной программы, выключите диспетчер и повторно запустите 
  установку программного обеспечения для клавиатуры Logitech G Series

****************************************************************************

11. СВЯЗЬ СО СЛУЖБОЙ ТЕХНИЧЕСКОЙ ПОДДЕРЖКИ

 Веб-узел: http://www.logitech.com

 На веб-узле Logitech содержится интерактивная служба технической поддержки, 
 пакеты исправлений,сведения о продукте и многое другое.

 FTP: ftp://ftp.logitech.com

 На FTP-узле можно найти пакеты исправлений и обновления драйверов для 
 продукции компании Logitech.

 Техническая поддержка в сети: http://www.logitech.com/support

 Свяжитесь с нашими специалистами по электронной почте и задайте им
 вопросы по технической поддержке.

 Техническая поддержка по телефону:
   +1 702-269-3457 (США) (Пн.-Пт., 6.00 - 18,00 тихоокеанское время)
   +1 416-207-2782 (Канада) (Пн.-Пт., 6.00 - 18,00 тихоокеанское время)
   (Примечание. Время работы службы может измениться)

   Сотрудники службы технической поддержки по телефону отвечают на вопросы, 
   связанные с технической поддержкой или техническим обслуживанием 
   продукции компании Logitech.

----------------------------------------------------------------------------
(c) Logitech, 2004-2006. Все права защищены.  Logitech, эмблема Logitech,
и другие товарные знаки Logitech являются собственностью компании Logitech и 
могут быть зарегистрированными товарными знаками.
Все остальные товарные знаки являются собственностью их владельцев.
